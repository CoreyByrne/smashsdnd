/*
	SCHEMAS.JS
	Contains the structure of each object in MongoDB.

	most of these are laid out in their individual class files (/public/javascript/class/...)
*/

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

Schemas = {}

Schemas.Tournament = new Schema({ // main thing that organizers create
	name: String // name of tournament (NOT ID)
	, creator: String // name of creator (NOT ID)
	, key: String // password to authenticate
	, date: Date // when is it?
	, created: Date // when was it created? (note diff from above)
	, location: String // self-explanatory
	, description: String // self-explanatory
	, games: [String] // strings of games that are in the tournament
	, acceptSetups: Boolean // whether to accept setups from players
	, events: [{type: Schema.ObjectId, ref: 'Event'}] // populatable event list
	, registrants: [{type: Schema.ObjectId, ref: 'Person'}] // populatable person list
	, setups: [{type: Schema.ObjectId, ref: 'Setup'}] // populatable setup list
});

Schemas.Event = new Schema({ // tournaments contain many events
	name: String // self-explanatory
	, game: String // game being played
	, description: String // self-explanatory
	, currentRound: Number // what current round the event is on (i.e. pools might be round 1, bracket round 2)
	, entranceFee: Number // self-explanatory
	, potBonus: Number // number to be added to event prize pool
	, payoutSchema: [Number] // percents that should add to 100%
	, tournament: {type: Schema.ObjectId, ref: 'Tournament'} // populatable tournament
	, seeding: [{type: Schema.ObjectId, ref: 'Person'}] // populatable person list (ordered by skill, best player is 1)
	, rounds: [{type: Schema.ObjectId, ref: 'Round'}] // populatable round list (i.e. two rounds of pools into bracket = 3)
	, state: String // either open, pending, active, complete
});

Schemas.Person = new Schema({ // tournaments/events contain many people, who own setups
	firstName: String // self-explanatory
	, lastName: String // self-explanatory
	, sponsor: String // self-explanatory
	, tag: String // self-explanatory
	, email: String // self-explanatory
	, owed: Number // net money owed by player (may be negative if they won payouts, positive if they just entered events)
	, events: [{type: Schema.ObjectId, ref: 'Event'}] // populatable event list that is entered
	//, challongeIDs: [Number] // same size as brackets
	, brackets: [{type: Schema.ObjectId, ref: 'Bracket'}] // populatable bracket list that is entered
})

Schemas.Setup = new Schema({ // tournaments/people have setups
	owner: {type: Schema.ObjectId, ref: 'Person'} // self-explanatory
	, friendlies: Boolean // friendlies setups are ones that are not being used for tournament (true if is friendlies)
	, name: String // setups can have names, i.e. stream setup
	, game: String // self-explanatory
	, match: {type: Schema.ObjectId, ref: 'Match'} // populatable match id (if being used for a match)
	, tournament: {type: Schema.ObjectId, ref: 'Tournament'} // populatable tournament id that owns this
})

Schemas.Round = new Schema({ // events have many rounds
	number: Number // which round number this is
	, event: {type: Schema.ObjectId, ref: 'Event'} // populatable event ID that owns this
	, bracketType: String // roundRobin/doubleElim/singleElim (type of bracket)
	, brackets: [{type: Schema.ObjectId, ref: 'Bracket'}] // populatable list of brackets this contains
	, entrantsPerBracket: Number // number of members in each bracket (i.e. 8 members of a single pool)
	, numberOfWinners: Number // self-explanatory (if 2 winners, don't play grand finals)
	, challongeType: String // challonge has their own naming conventions (i.e. double elimination instead of doubleElim)
	, complete: Boolean // is this round complete? (yes if all brackets are complete)
	, seeding: [{type: Schema.ObjectId, ref: 'Person'}] // sorted ordering of players within a round
})

Schemas.Bracket = new Schema({ // rounds have many brackets
	bracketType: String // roundRobin/doubleElim/singleElim (type of bracket)
	, complete: Boolean // are all matches within bracket complete?
	, numberOfWinners: Number // self-explanatory (if 2 winners, don't play grand finals)
	, round: {type: Schema.ObjectId, ref: 'Round'} // populatable round that owns this
	, matches: [{type: Schema.ObjectId, ref: 'Match'}] // populatable matches that this contains
	, players: [{type: Schema.ObjectId, ref: 'Person'}] // populatable players list within this
})

Schemas.Match = new Schema({ // brackets have many matches
	state: String // either open, pending, complete or in progress. first 3 are supported by challonge, last one is just for us
	, round: Number // NOT THE SAME ROUNDS IN THIS PROGRAM. this is a challonge based round (losers round 1 = -1, winners round 3 = 3)
	, started: String // date of start
	, players: [{type: Schema.ObjectId, ref: 'Person'}]// populatable list of players (size 2, each could be null) in match
	, bracket: {type: Schema.ObjectId, ref: 'Bracket'} // populatable bracket that owns this
	, scoresCsv: String // score results, i.e. "3-0"
	, isGrandFinals: Boolean
	, prereqMatches: [{type: Schema.ObjectId, ref: 'Match'}] // matches that feed into this
	, playerLostPrereq: [Boolean] // is this losers bracket?
	, setup: {type: Schema.ObjectId, ref: 'Setup'} //setup on which this match is being played
})

module.exports = Schemas;