/*
	Challonge Unit Cases
	NOT EXPLICITLY CALLED ANYMORE, was used to test challonge functionality 
*/

var runChallongeTest = function()
{
	var challonge = require('./node-challonge');
	var client = challonge.createClient({
		apiKey: 'JSXVEvS3ouvK01SVPGs6UGbQ7zMCgviD3x7GfFhZ',
		format: 'json',
		version: 1,
	});

	var tourneyName='asdfjhsjfhskdhfdds';
	client.tournaments.create({
		tournament: {
			name: 'name-' + tourneyName,
			url: tourneyName,
			signupCap: 256,
			tournamentType: 'single elimination',
		},
		callback: function(err,data){
			if (err) { console.log(err); return; }
			//console.log(data);
		}
	});
	client.tournaments.show({
		id: tourneyName,
		callback: function(err,data){
			if (err) { console.log(err); return; }
			console.log(data);
		}
	});
	/*client.tournaments.update({
		id: tourneyName,
		tournament: {
			name: 'renamed test tournet'
		},
		callback: function(err,data){
			if (err) { console.log(err); return; }
			//console.log(data);
		}
	});*/
	for (var i = 0; i < 256; i++) {
		client.participants.create({
			id: tourneyName,
			participant: {
				name: 'test player ' + i
			},
			callback: function(err,data){
				if (err) { console.log(err); return; }
				//console.log(data);
			}
		});
	}

	client.tournaments.start({
		id: tourneyName,
		callback: function(err,data){
			if (err) { console.log(err); return; }
			//console.log(data);
		}
	});

	client.participants.index({
		id: tourneyName,
		callback: function(err,data){
			if (err) { console.log(err); return; }
			console.log(data);
		}
	});
	client.matches.index({
		id: tourneyName,
		callback: function(err,data){
			if (err) { console.log(err); return; }
			console.log(data);
			for (var i = 0; i < data.length; i++) {
				client.matches.update({
				id: tourneyName,
				matchId: data[i].match.id,
				match: {
					scoresCsv: '3-0',
					winnerId: data[i].match.player1Id
				},
				callback: function(err,data){
					if (err) { console.log(err); return; }
					console.log(data);
				}
	});
			}
		}
	});
}

module.exports = {
    runChallongeTest: runChallongeTest
};