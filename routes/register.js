/*
 *
 *   Create route
 *  
 *  Express route which listens on /register
 *
 *  All pages for registering a player
 *  listen on this route
 *
 *
 */

 var express = require('express');
var router = express.Router();

var Director = require('./Director');

//render the registration view for a given tournament
router.get('/:t_id', function(req, res) {
	Director(req.Class.Database, { //get the tournament specified
		command: "Get"
		, structure: "Tournament"
		, populate: ["events"]
		, id: req.params.t_id
	}, function (json) {
		var anyToDisplay = false
		json.events.forEach(function(event, i) {
			event.display = event.state=="pending";
			anyToDisplay = anyToDisplay || event.display;
		});
		json.anyToDisplay = anyToDisplay;
		res.render('register-player', { //render page
			title: 'Register for ' + json.name
			, description: 'Registration for ' + json.name
			, tournament: json
			, customJS: [
				{js: 'pages/register-player.js'}
				, {js: 'dynamic-list.js'}
			]
		});
	}, Director.renderCallback("500", res));
});

//register a person and their setups for a given tournament
router.post('/:t_id', function(req, res) {
	var p = new req.Class.Person(req.body);
	Director(req.Class.Database, { //add the person to the tournament
		command: "Add"
		, structure: "Person"
		, object: p
		, id: req.params.t_id
		, extra: req.body.events
	}, function(err) {
		if(req.body.setups != undefined && req.body.setups.length != 0) {
			req.Class.Database.AddSetups ( //add the person's setups to the tournament
				req.body.setups 
				, req.Class.Setup
				, p._id
				, req.params.t_id
				, function() {
					res.send(p.CreateExport());			
			});
		} else {
			res.send(p.CreateExport());
		}
	}, Director.renderCallback("500", res));
});

module.exports = router;
