/*
 *
 *   View route
 *  
 *  Express route which listens on /view
 *
 *  All pages for viewing brackets and submitting individual matches
 *  listen on this route
 *
 *
 */

 var express = require('express');
var router = express.Router();

var Director = require('./Director');

//render view page for all tournaments
router.get('/', function(req, res) {
	Director(req.Class.Database, {
		command: 'Get'
		, structure: 'All'
	}, function (json) {
		res.render('tournamentlist', {
			title: 'Tournament Listing',
			description: 'List of current tournaments.',
			tournament: json
		});
	}, Director.renderCallback("404", res));
});

//render view page for specified tournament
router.get('/t/:id', function(req, res){
	Director(req.Class.Database, {
		command: "Get"
		, structure: "Tournament"
		, populate: ["events", "setups", "registrants", "events.rounds", "events.rounds.brackets"]
		, id: req.params.id
	}, function (json) {
		res.render('view-tournament', {
			title: json.name
			, tournament: json
		});
	}, Director.renderCallback("404", res));
});

//render view page for specified event
router.get('/e/:id', function(req, res) {
	Director(req.Class.Database, {
		command: 'Get'
		, structure: 'Event'
		, populate: [ "seeding", "rounds" ,"rounds.brackets", "rounds.brackets.players"]
		, id: req.params.id
	}, function (json) {
		console.log(json.rounds[0]);
		res.render('view-event', {
			title: json.name
			, event: json
		});
	}, Director.renderCallback("404", res));
});

//render view page for specified round
router.get('/r/:id', function(req, res) {
	Director(req.Class.Database, {
		command: "Get"
		, structure: "Round"
		, populate: ["brackets", "brackets.players", "event", "event.tournament"]
		, id: req.params.id
	}, function (json) {
		res.render('view-round', {
			title: json.event.name + ", Round " + round.number
			, round: json
		});
	}, Director.renderCallback("404", res));
});

//render view and submission page for specified bracket
router.get('/b/:id', function(req, res) {
	Director.GetViewableBracketJSON(req.Class.Database, req.params.id, function(err, json) {
		var round_robin = json.type && json.type == "round robin";
		var stringifiedOutput = JSON.stringify(json);
		res.render('view-bracket', {
			title: "Bracket Viewer"
			, description: "View bracket."
			, bracketId: req.params.id
			, roundRobin: round_robin
			, bracket: round_robin ? json : stringifiedOutput
			, customStylesheet: [
				{css: 'bracket.css'}
				, {css: 'matches.css'}
				, {css: 'modal.css'}
				, {css: 'matches.css'}
			]
			, customJS: [ //open source tool for displaying brackets
				{js: 'd3.min.js'}
				, {js: 'd3-bracketer.js'}
				, {js: 'pages/view-bracket.js'}
			]
		});
	});
});
module.exports = router;
