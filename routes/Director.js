/*
 * Database: pass req.Class.Database to the director
 * info: arguments for the call
 * callback: function (doc), called on success
 * errorCallback: function (), called on failure
 * 
 * info: {
 *	 command: "Get"
 *   , structure: "Tournament"
 *	 , populate: ["events", "events.seeding"]
 *	 , id: _id
 * }
 *
 * info: {
 *	 command: "Add"
 *   , structure: "Event"
 *	 , object: {}
 *	 , id: _id
 * }
 *
 * or
 *
 * info: {
 *	 command: "Update"
 *   , structure: "Round"
 *	 , object: {}
 *	 , id: _id
 * }
 *
 *
 */

function Director(Database, info, callback, errorCallback)
{
	// this is the callback for the error function, which allows us to split callbacks
	function catchError(err, doc) {
		if(err || !doc) {
			// really useful print, okay outside dev environment
			console.log("Director got a database error,", err);
			errorCallback();
		} else {
			callback(doc);
		}
	}
	
	// list of valid commands for the Director
	var commands = [
		"Get", "Update", "UpdatePush", "Add"
	];
	
	// list of valid structures
	var structures = [
		"Tournament", "Event", "Round"
		, "Bracket" , "Match", "Person"
		, "Setup"
	];
	if(info.command == "Get" && info.structure == "All") {
		Database.GetTournaments(catchError, info.populate);
	}
	else if(info.command == "Update" && info.structure == "All") {
		console.log("!!!! CANNOT UPDATE ALL TOURNAMENTS !!!!");
	}
	else if(commands.indexOf(info.command) == -1) {
		console.log("!!!! INVALID COMMAND !!!!", info.structure);
	}
	else if(structures.indexOf(info.structure) == -1) {
		console.log("!!!! INVALID STRUCTURE !!!!", info.structure);
	}
	else {
		// database[command] with structure exists so we call it
		Database[info.command](
			info.structure
			, catchError
			, info.id
			, info.populate || info.object
			, info.extra);
	}
}

// director function to get tournament and return specific JSON for bracket viewer on front end

/*
	SAMPLE OUTPUT:
	{
		round: 5
		name: grand finals
		players: [{id: personExports, challongeID: challongeID}, ... ]
		state: open/pending/active/complete
		scoresCSV: "3-0"
		setup: setup id
		started: date
		id: ..
		challongeID: ..
		children: [
			{
				round: 4
				name: winners round 4
				players: [{...}]
				state: ...
				id...
				challongeid..
				children: []
			}
		]
	}

	note that the internal construction is based off of name and children

*/

// sets data from FROM to TO. helper function (DON'T CALL; USED IN GETVIEWABLEBRACKETJSON)
Director.SetBracketData = function(from, to) {
	to.round = from.round;
 	to.scoresCsv = from.scoresCsv;
 	//console.log("scoresCSV = " + to.scoresCsv);
 	to.state = from.state;
 	to.started = from.started;
 	to.players = from.players;
 	to.id = from._id;
 	to.challongeID = from.challongeID;	
 	to.setup = from.setup;
}


// helper function that returns [children], used by GetViewableBracketJSON (DON'T CALL)
Director.RecursiveMatchJSONBuilder = function (MatchHashMap, mID) {
	var Dir = this;
	var output = [];
	var this_round = MatchHashMap[mID].round;
	MatchHashMap[mID].prereqMatches.forEach(function (preID) {
		var matchConstruct = {};
		/*
			one thing that is important is that despite the fact that losers matches feed from winners and losers matches
			you do not want to have a winners bracket match as a losers bracket match's child
			that would not make for a coherent graph
		*/
		if (preID != null && 
			// check same signs
			((MatchHashMap[preID].round > 0 && this_round > 0) || (MatchHashMap[preID].round < 0 && this_round < 0))) {
			Dir.SetBracketData(MatchHashMap[preID], matchConstruct);
			matchConstruct.children = Dir.RecursiveMatchJSONBuilder(MatchHashMap, preID); // recursively get children
		 	if (MatchHashMap[preID].round > 0) { // round #s tell us whether its winners or losers
		 		matchConstruct.name = "Winners Round " + MatchHashMap[preID].round;
		 		output.push(matchConstruct);
		 	}
			else {
				matchConstruct.name = "Losers Round " + -1*MatchHashMap[preID].round;
				output.push(matchConstruct);
			}
		}
	});
	return output;
}

// returns the prepared bracket json, the large comment block above the two helper functions above
// refers to the output from this function
Director.GetViewableBracketJSON = function(Database, b_id, callback) {
	var Dir = this;
	
	// the output container
	var OutputJSON; 
	// get the bracket
	Database.Get('Bracket', function(err, b_data) {
		if(err) console.log(err);
		// round robin is a matrix, not a bracket
		if (b_data.bracketType == "round robin") {
			OutputJSON = {}
			// output json is a player by player matrix
			OutputJSON.matches = new Array(b_data.players.length);
			for(var i = 0; i < OutputJSON.matches.length; i++) {
				OutputJSON.matches[i] = new Array(b_data.players.length);
				for(var j = 0; j < OutputJSON.matches.length; j++) {
					OutputJSON.matches[i][j] = false;
				}
			}
			OutputJSON.players = []
			// we store index for later
			var indices = {};
			// for each player
			b_data.players.forEach(function(p, i) {
				// invert to hash to index
				indices[p] = i;
				OutputJSON.players.push(p);
			});
			
			// go through the matches
			b_data.matches.forEach(function(m_data) {
				// get the indices of the player
				var i1 = indices[m_data.players[0]._id],
					i2 = indices[m_data.players[1]._id];
					
				// we do the smaller one first
				var k1 = i1 < i2 ? i1: i2,
					k2 = i1 >= i2 ? i1: i2;
					
				OutputJSON.players[i1] = m_data.players[0];
				OutputJSON.players[i2] = m_data.players[1];
				OutputJSON.matches[i1].player = m_data.players[0];
				OutputJSON.matches[i2].player = m_data.players[1];
					
				// store the json
				OutputJSON.matches[k2][k1] = m_data;
			});
			
			OutputJSON.matches.shift();
			OutputJSON.players.pop();
			OutputJSON.matches = OutputJSON.matches.reverse();
			
			OutputJSON.type = "round robin";
		} else {
			OutputJSON = {};// handle single elim and double elim
			OutputJSON.name = 'Grand Finals';
			var MatchHashMap = {};
			var GrandFinalsMatches = {};
			b_data.matches.forEach(function(match) {
				MatchHashMap[match._id] = match; // first, hash every match based off of id <-- does this line work
				GrandFinalsMatches[match._id] = match;
			})
			for (var match in MatchHashMap) { // determine which matches are "grand finals matches" by finding which have no dependencies
			  if (MatchHashMap.hasOwnProperty(match)) {
			    MatchHashMap[match].prereqMatches.forEach(function(prereq) {
					delete GrandFinalsMatches[prereq]; // if it has a dependency, delete the dependency from the grand finals list
				})
			  }
			}	 	

			for (var mID in GrandFinalsMatches) { // hopefully this is only being called once, which is TRUE for single/double elim
				if (GrandFinalsMatches.hasOwnProperty(mID)) {
					//console.log(OutputJSON);
					Dir.SetBracketData(GrandFinalsMatches[mID], OutputJSON);
					// there is an edge case, currently grand finals set 2 has both dependencies from grand finals set 1,
					// we need to handle single elim and double elim differently
					//console.log(b_data);
					if (b_data.bracketType == "double elimination") {
						OutputJSON.name = "Grand Finals Set 2"
						var gf_set1 = {name: "Grand Finals Set 1", children: []};
						Dir.SetBracketData(MatchHashMap[MatchHashMap[mID].prereqMatches[0]], gf_set1); // ignore double dependency
						MatchHashMap[MatchHashMap[mID].prereqMatches[0]].prereqMatches.forEach(function(inner_mID) {
							var new_final = {};
							Dir.SetBracketData(MatchHashMap[inner_mID], new_final);
							if (new_final.round > 0) new_final.name = "Winners Finals"; 
							else new_final.name = "Losers Finals";
							new_final.children = Dir.RecursiveMatchJSONBuilder(MatchHashMap, inner_mID);
							gf_set1.children.push(new_final);
						})
						OutputJSON.children = [gf_set1];
					}
					else {
					 	OutputJSON.children = Dir.RecursiveMatchJSONBuilder(MatchHashMap, mID);
					}
				}
			}
		}
		callback(null, OutputJSON);
	}, b_id, ['matches', 'matches.players']);
}

// validates a tournament
Director.Validate = function(t, key, id) {
	return typeof key !== 'undefined'
		&& typeof id !== 'undefined'
		&& key == t.key
		&& id == t._id 
		&& t.key != "NoKey";
}

// create an error code callback
Director.renderCallback = function(status, res) {
	return function() {
		res.status(parseInt(status));
		res.render(status);
	}
}

// used to respond to illegal frontend actions
Director.accessDenied = function(url, res) {
	res.render('login-tournament', {
		title: '403 Access Denied'
		, url: url
		, customStylesheet: [
			{css: 'login-tournament.css'}
		]
		, customJS: [
			{js: 'pages/login-tournament.js'}
		]
	});
}

module.exports = Director;
