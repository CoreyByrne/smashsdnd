/*
 *
 *   Index route
 *  
 *  Express route which listens on /
 *
 *  TOryaa homepage listens on this route
 *
 *
 */

var express = require('express');
var router = express.Router();

var Director = require('./Director');

//renders this index
router.get('/', function(req, res, next) {
  res.render('index', {
		title: 'TOryaa',
		description: 'A tournament organizer application.'
	});
});

//route used to update matches
router.post('/submit', function(req, res) {	
	Director(req.Class.Database, {
		command: "Update"
		, structure: "Match"
		, id: req.body.m_id
		, object: {
			state: "complete"
			, scoresCsv: req.body.score
		}
		, extra: {
			challongeID: req.body.matchChallongeID
			, update: {
				scoresCsv: req.body.score
				, winnerId: req.body.winnerId
				, loserId: req.body.loserId
			}
			, b_id: req.body.b_id
			, Class: req.Class
			, key: req.body.key
		}
	}, function(json) {
		if(req.body.s_id) {
			Director(req.Class.Database, {
				command: "Update"
				, structure: "Setup"
				, id: req.body.s_id
				, object: {
					match: undefined
				}
			}, function(json) {
				res.send(json);
			}, Director.renderCallback("500", res));
		} else {
			res.send(json);
		}
	}, Director.renderCallback("500", res));
});

module.exports = router;