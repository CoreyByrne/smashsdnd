/*
 * Route for temporary testing use
 *
      _____________
     /             \
    /               \
   /       ___       \
  /   ___ / S \ ___   \
 /   / S \\___// K \   \
 \   \___/     \___/   /
  \___________________/
    \  |  \| |/  |  /
     \ |         | /
      \|         |/
*/
var express = require('express');
var router = express.Router();
var Director = require('./Director');

// DEMO TEMP CREATE TO CREATE SHREK SUPERSLAM DEMO
router.get('/shrek', function(req, res) {
	var Class = req.Class;
	console.log("create shrek superslam tournaments");
	var tournamentList = ["Shrekt", "Super Shrekt", "Shrek 1", "Shrek 2", "Shrek the Third"];	
	var nameList = ["Princess Fiona", "Puss in Boots", "Dronkey", "Shrek"]

	var t_saves = tournamentList.length;
	tournamentList.forEach(function(tlist_it) {
		var t = new Class.Tournament({
			name:tlist_it
			, creator: "Donkey"
			, key: "a"
			, description: ""
			, acceptSetups: true
			, games: ["Shrek Superslam"]
			, location: "RPI"
			, date: "2016-04-26T21:22:56.418-04:00"
		});
		Class.Database.Add('Tournament', function(err, t_data) {
			if (err) console.log(err);
			else {
				t_saves--;
				var numberOfTestEvents = 4; // customize these
				var numberOfTestRounds = 1;
				//var numberOfTestPlayers = 4;
				var e_saves = numberOfTestEvents;
				var eventIds = [];
				for (var i = 0; i < numberOfTestEvents; i++) {
					var i_bound = i;
					var e = new Class.Event({
						name: "Shrek " + (i+1)  + "v" + (i+1)
						, game: t_data.games[i % t_data.games.length]
						, description: "description"
						, tournament: t_data._id
						, potBonus: 100000
						, entranceFee: 5000
						, payoutSchema: [75,25] 
					});
					var rounds = [];
					for (var j = 0; j < numberOfTestRounds; j++) {
						var roundData = {};
						roundData.entrantsPerBracket = 10; // customize this, should be > 2
						roundData.numberOfWinners
						roundData.number = j+1;
						if (j+1 == 1 && numberOfTestRounds != 1) roundData.bracketType = "RoundRobin"; // typical RR --> DE style
						else roundData.bracketType = "DoubleElimBracket"; // switch as necessary, or even change round #s
						rounds.push(roundData);
					}
					Class.Database.Add('Event', function(err, e_data) {
						//console.log(e);
						eventIds.push(e._id);
						//console.log("eIds");
						//console.log(eventIds);
						if (err) console.log(err);
						e_saves--;
						if (e_saves == 0) {
							var k_saves = nameList.length;
							nameList.forEach(function(nlist_it) {
								//var k_bound = k;
								var p = new Class.Person({
									firstName: nlist_it
									, lastName: " "
									, sponsor: "Shrek"
									, tag: nlist_it
									, email: nlist_it + "@shrek.com"
									, owed: e.entranceFee*numberOfTestEvents
									, tournament: t_data._id
									, events: eventIds
								});
								Class.Database.AddPerson(p, function(err, p_data) {
									if (err) console.log(err);
									
									--k_saves;
									if(k_saves == 0) {
										
										gamesToAdd = [];
										for(var l = 0; l < numberOfTestEvents; ++l) {
											for(var m = 0; m < nameList.length; ++m) {
												gamesToAdd.push(t.games[l])
											}
										}
										
										Class.Database.AddSetups(gamesToAdd, Class.Setup, p_data._id, t_data._id, function(err, s_data) {
											if (tlist_it == tournamentList[tournamentList.length-1]) res.render('shrek');
											// res.send('Successfully created a successful tournament with ' + numberOfTestEvents + ' event(s), '
											// + numberOfTestRounds + ' round(s), ' + numberOfTestPlayers + ' players'); // finish creation
										});
									}
								}, t_data._id, eventIds);
							})
						}
					}, t_data._id, e, rounds);
				}
			}
		}, null, t);
	});
})


// small temporary view that creates a customizable tournament with events, rounds, players etc
// all the numbersOfX are customizable and work alright. this is a mock object that is used for testing
// warning that because mutex might not be honored here, errors could occur with internal bindings
router.get('/create', function(req, res) {
	var Class = req.Class;
	console.log("COMMENTING @TEST 1: create new tournament with 10 players");
	var t = new Class.Tournament({
			name:"tournament name"
			, creator: "creator"
			, key: "a"
			, description: "description"
			, acceptSetups: true
			, games: ["melee", "brawl", "PM"]
			, location: "location"
			, date: "2016-04-26T21:22:56.418-04:00"
		});
	Class.Database.Add('Tournament', function(err, t_data) {
		if (err) console.log(err);
		else {
			var numberOfTestEvents = 1; // customize these
			var numberOfTestRounds = 1;
			var numberOfTestPlayers = 3;
			var e_saves = numberOfTestEvents;
			var eventIds = [];
			for (var i = 0; i < numberOfTestEvents; i++) {
				var i_bound = i;
				var e = new Class.Event({
					name: "event name " + i
					, game: t_data.games[i % t_data.games.length]
					, description: "description"
					, tournament: t_data._id
					, potBonus: 100
					, entranceFee: 5
					, payoutSchema: [75,25] 
				});
				var rounds = [];
				for (var j = 0; j < numberOfTestRounds; j++) {
					var roundData = {};
					roundData.entrantsPerBracket = 10; // customize this, should be > 2
					roundData.numberOfWinners
					roundData.number = j+1;
					if (j+1 == 1 && numberOfTestRounds != 1) roundData.bracketType = "RoundRobin"; // typical RR --> DE style
					else roundData.bracketType = "DoubleElimBracket"; // switch as necessary, or even change round #s
					rounds.push(roundData);
				}
				Class.Database.Add('Event', function(err, e_data) {
					eventIds.push(e._id);
					if (err) console.log(err);
					e_saves--;
					if (e_saves == 0) {
						var k_saves = numberOfTestPlayers;
						for (var k = 0; k < numberOfTestPlayers; k++) {
							var k_bound = k;
							var p = new Class.Person({
								firstName: "firstName " + k
								, lastName: "lastName " + k
								, sponsor: "sponsor"
								, tag: "tag " + k
								, email: "test@email.com"
								, owed: e.entranceFee*numberOfTestEvents
								, tournament: t_data._id
								, events: eventIds
							});
							Class.Database.AddPerson(p, function(err, p_data) {
								if (err) console.log(err);
								
								--k_saves;
								if(k_saves == 0) {
									
									gamesToAdd = [];
									for(var l = 0; l < numberOfTestEvents; ++l) {
										for(var m = 0; m < numberOfTestPlayers; ++m) {
											gamesToAdd.push(t.games[l])
										}
									}
									
									Class.Database.AddSetups(gamesToAdd, Class.Setup, p_data._id, t_data._id, function(err, s_data) {
										res.send('Successfully created a successful tournament with ' + numberOfTestEvents + ' event(s), '
										+ numberOfTestRounds + ' round(s), ' + numberOfTestPlayers + ' players'); // finish creation
									});
								}
							}, t_data._id, eventIds);
						}
					}
				}, t_data._id, e, rounds);
			}
		}
	}, null, t);
})

router.get('/', function(req, res){ // replace this with whatever functionality you want to test
	Director.GetViewableBracketJSON(req.Class.Database, "57218743a0363b9c5a3be490", function(err, data) {
		if (err) {console.log(err); return;}
		//console.log(data);
	});
	/*Director(req.Class.Database, {
		command: 'Get'
		, structure: 'All'
		, id: ''
		, populate: ['events', 'events.rounds', 'events.rounds.brackets', 'events.rounds.brackets.matches']
	}, function (a) {
		b_id = a[0].events[0].rounds[0].brackets[0]._id;
		
		req.Class.Challonge.GetChallongeBracketMatches(b_id, req.Class, function() {
			
			
			res.send('ack')
		});
		//res.send('ack')
	}, function(err) {
		console.log('asplode');
		res.send('asplode')
	});*/
});

router.post('/', function(req, res){ // use to test very specific functionality
	s = []
	te = 13;
	for(i = 0; i < te; ++i) {
		s.push(i)
	}
	epb = 4;
	nb = Math.ceil(te / epb);
	console.log(nb)
	o = [];
	for(i = 0; i < nb; ++i) {
		//o.push([]);
		o[i] = []
	}
	
	for(i = 0; i < te; ++i) {
		o[i % nb].push(s[i]);
	}
	//if(o2.length != 0) o.push(o2);
	
	console.log(o);
	
	res.send('ack');
});

module.exports = router;