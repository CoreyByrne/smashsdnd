/*
 *
 *   Edit route
 *  
 *  Express route which listens on /edit
 *
 *  All pages for editing database entrys
 *  listen on this route
 *
 *
 */

var express = require('express');
var router = express.Router();

var Director = require('./Director');

// identical to the typical tournament view
router.get('/', function(req, res) {
	Director(req.Class.Database, {
		command: "Get"
		, structure: "All"
	}, function (json) {
		res.render('edit-list', {
			title: 'Tournament Listing',
			tournament: json
		});
	}, Director.renderCallback("404", res));
});

// push updates to a tournament
router.get('/:t_id', function(req, res) {
	// we need to get the tournament to validate 
	Director(req.Class.Database, {
		command: "Get"
		, structure: "Tournament"
		, populate: ["events"]
		, id: req.params.t_id
	}, function (json) {
		// construct and validate
		t = new req.Class.Tournament(json);
		if(Director.Validate(t, req.query.key, req.params.t_id)) {
			// if validate render edit tournament
			res.render('edit-tournament', {
				title: json.name
				, tournament: json
				, customJS: [
					{js: 'pages/edit-tournament.js'}
					, {js: 'bootstrap-datepicker.min.js'}
					, {js: 'dynamic-list.js'}
				]
			});
		} else {
			Director.accessDenied('/edit/' + req.params.t_id + '?key=', res);
		}
	}, Director.renderCallback("404", res));
});

// edit event view
router.get('/:t_id/:e_id', function(req, res) {
	// we populate on tournament so we can validate
	Director(req.Class.Database, {
		command: "Get"
		, structure: "Event"
		, populate: ["tournament"]
		, id: req.params.e_id
	}, function (json) {
		// construct and validate
		t = new req.Class.Tournament(json.tournament);
		if(Director.Validate(t, req.query.key, req.params.t_id)) {
			res.render('edit-event', {
				title: json.name
				, tournament: json.tournament
				, event: json
				, customJS: [
					{js: 'pages/edit-event.js'}
				]
			});
		} else {
			Director.accessDenied('/edit/' + req.params.t_id + "/" + req.params.e_id + '?key=', res);
		}
	}, Director.renderCallback("404", res));
});

// edit seeding view
router.get('/:t_id/:e_id/seeding', function(req, res) {
	// get tournament for validation and seeding to display seeds
	Director(req.Class.Database, {
		command: "Get"
		, structure: "Event"
		, populate: ["tournament", "seeding"]
		, id: req.params.e_id
	}, function (json) {
		// construct and validate
		t = new req.Class.Tournament(json.tournament);
		if(Director.Validate(t, req.query.key, req.params.t_id)) {
			res.render('edit-seeding', {
				title: json.name
				, event: json
				, customStylesheet: [
					{css: 'edit-seeding.css'}
				]
				, customJS: [
					{js: 'pages/edit-seeding.js'}
				, {js: 'jquery-sortable-min.js'}
				]
			});
		} else {
			Director.accessDenied('/edit/' + req.params.t_id + "/" + req.params.e_id + '/seeding?key=', res);
		}
	}, Director.renderCallback("404", res));
});

// editing a tournament
router.post('/:t_id', function(req, res) {
	// get for validate
	Director(req.Class.Database, {
		command: "Get"
		, structure: "Tournament"
		, populate: ["events"]
		, id: req.params.t_id
	}, function (json) {
		// constuct and validate
		t = new req.Class.Tournament(json);
		if(Director.Validate(t, req.query.key, req.params.t_id)) {
			// update tournament
			Director(req.Class.Database, {
				command: "Update"
				, structure: "Tournament"
				, id: req.params.t_id
				, object: req.body
			}, function(obj) {
				// send ack
				res.send({});
			}, Director.renderCallback("404", res));
		} else {
			Director.renderCallback("403", res)();
		}
	}, Director.renderCallback("404", res));
});

// edit event
router.post('/:t_id/:e_id', function(req, res) {
	// get for validate
	Director(req.Class.Database, {
		command: "Get"
		, structure: "Event"
		, populate: ["tournament"]
		, id: req.params.e_id
	}, function (json) {
		// construct and validate
		t = new req.Class.Tournament(json.tournament);
		if(Director.Validate(t, req.query.key, req.params.t_id)) {
			// update event
			Director(req.Class.Database, {
				command: "Update"
				, structure: "Event"
				, id: req.params.e_id
				, object: req.body
			}, function(obj) {
				// send ack
				res.send({});
			}, Director.renderCallback("404", res));
		} else {
			Director.renderCallback("403", res)();
		}
	}, Director.renderCallback("404", res));
});

// seeding is tournament event
router.post('/:t_id/:e_id/seeding', function(req, res) {
	req.Class.Database.UpdateSeeding(
		req.params.t_id
		, req.params.e_id
		, req.body.seeds
	);
	res.send({});
});

module.exports = router;
