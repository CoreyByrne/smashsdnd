/*
 *
 *   Create route
 *  
 *  Express route which listens on /create
 *
 *  All pages for creating database entries
 *  listen on this route
 *
 *
 */

var express = require('express');
var router = express.Router();

var Director = require('./Director');

router.get('/', function(req, res) {
	// render the basic tournament creation page
	res.render('create-tournament', {
		title: 'Create a tournament'
		, description: 'Create a new tournament.'
		, customStylesheet: [
			{css: 'bootstrap-datepicker3.min.css'}
			, {css: 'dynamic-setups.css'}
		]
		, customJS: [
			{js: 'pages/create-tournament.js'}
			, {js: 'bootstrap-datepicker.min.js'}
			, {js: 'dynamic-list.js'}
		]
	});
});

router.post('/', function(req, res) {
	// we expect a tournament, so we validate it here
    var t = new req.Class.Tournament(req.body);
    // we as the director to add it to the database
	Director(req.Class.Database, {
		command: "Add"
		, structure: "Tournament"
		, object: t
	}, function(json) {
		// when we finish, we send the output to the frontend
		res.send(json);
	},  Director.renderCallback("500", res));
});

router.get('/:t_id', function(req, res) {
	// we will need a tournament to validate
	Director(req.Class.Database, {
		command: 'Get'
		, structure: 'Tournament'
		, id: req.params.t_id
	}, function (json) {
		// we construct the tournament to validate 
		var t = new req.Class.Tournament(json);
		if(Director.Validate(t, req.query.key, req.params.t_id)) {
			// if they've validated we can render the create event
			res.render('create-event', {
				title: 'Create a new event'
				, description: 'Create an event for this tournament.'
				, tournament: t.CreateExport(withKey = false, withID = true)
				, brackets: [
					{
						functionName: "DoubleElimBracket"
						, prettyName: "Double Elimination"
					} , {
						functionName: "RoundRobin"
						, prettyName: "Round Robin"
					} , {
						functionName: "SingleElimBracket"
						, prettyName: "Single Elimination"
					}
				] , customJS: [
					{js: 'pages/create-event.js'}
				]
			});
		} else {
			// access denied because they didn't validate
			Director.accessDenied('/create/' + req.params.t_id + '?key=', res);
		}
	}, Director.renderCallback("404", res));
});

router.post('/:t_id', function(req, res) {
	// we will need a tournament to validate
	Director(req.Class.Database, {
		command: "Get"
		, structure: "Tournament"
		, id: req.params.t_id
	}, function (json) {
		// we construct the tournament to validate 
		var t = new req.Class.Tournament(json);
		if(Director.Validate(t, req.query.key, req.params.t_id)) {
			// if they've validated can save the event
            var e = new req.Class.Event(req.body); // construct the event
			// now we ask director to add the event
			Director(req.Class.Database, {
				command: "Add"
				, structure: "Event"
				, id: req.params.t_id
				, object: e
				, extra: req.body.roundData
			}, function(obj) {
				// this function is called when all the rounds are added
				res.send(e.CreateExport());
			}, Director.renderCallback("500", res));
		} else {
			// here we just render of 403 forbidden, cuz its a post
			Director.renderCallback("403", res)();
		}
	}, Director.renderCallback("404", res));
});


module.exports = router;
