/*
 *
 *   Manage route
 *  
 *  Express route which listens on /manage
 *
 *  All pages for managing existing unstarted events and started brackets
 *  listen on this route
 *
 *
 */

 var express = require('express');
var router = express.Router();

var Director = require('./Director');

//renders the manage tournament view
router.get('/', function(req, res) {
	Director(req.Class.Database, {
		command: 'Get'
		, structure: 'All'
	}, function (json) {
		res.render('manage-list', {
			title: 'Tournament Listing',
			description: 'List of current tournaments.',
			tournament: json
		});
	}, Director.renderCallback("404", res));
});

//renders the management view for a given tournament
router.get('/:t_id', function(req, res) {
	Director(req.Class.Database, { //get the specified tournament
		command: "Get"
		, structure: "Tournament"
		, populate: ["events", "setups", "registrants"]
		, id: req.params.t_id
	}, function (json) {
		t = new req.Class.Tournament(json);
		if(Director.Validate(t, req.query.key, req.params.t_id)) { //valid key submitted
			res.render('manage-tournament', {
				title: json.name
				, tournament: json
			});
		} else { //invalid key submitted
			Director.accessDenied('/manage/' + req.params.t_id + '?key=', res);
		}
	}, Director.renderCallback("404", res));
});

//renders the management view for a given event in a given tournament
router.get('/:t_id/e/:e_id', function(req, res) {
	Director(req.Class.Database, { //get the tournament, event, setups, seeding, rounds, brackets, and players specified
		command: "Get"
		, structure: "Event"
		, populate: ["tournament", "tournament.setups", "seeding", "rounds", "rounds.brackets", "rounds.brackets.players"]
		, id: req.params.e_id
	}, function (json) {
		var t = new req.Class.Tournament(json.tournament);
		if(Director.Validate(t, req.query.key, req.params.t_id)) { //valid key submitted
			var e = new req.Class.Event(json);
			if(e.state == "pending") { //if event hasn't started yet
				var s = json.tournament.setups;
				
				for(var i = s.length-1; i >= 0; i--) {
					if(s[i].game != json.game)
						s.splice(i, 1);
				}
				
				res.render('start-event', { //manage page
					title: json.name
					, tournament: json.tournament
					, event: json
					, customJS: [
						{js: 'pages/start-event.js'}
					]
				});
			} else { //event has started
				var currentRound;
				json.rounds.forEach(function(round) {
					if(round.number == json.currentRound)
						currentRound = round;
				});
				
				var s = json.tournament.setups;
				
				for(var i = s.length-1; i >= 0; i--) {
					if(s[i].game != json.game)
						s.splice(i, 1);
				}
				
				res.render('manage-event', { //manage page
					title: json.name
					, tournament: json.tournament
					, event: json
					, round: currentRound
					, customJS: [
						{js: 'pages/manage-event.js'}
					]
				});
			}
		} else { //invalid key submitted
			res.render('login-tournament', {
				title: '403 Access Denied'
				, url: '/manage/' + req.params.t_id + "/" + req.params.e_id + '?key='
				, customStylesheet: [
					{css: 'login-tournament.css'}
				]
				, customJS: [
					{js: 'pages/login-tournament.js'}
				]
			});
		}
	}, Director.renderCallback("404", res));
});

//renders the management view for a given bracket within a given tournament
router.get('/:t_id/b/:b_id', function(req, res) {
	Director(req.Class.Database, { //get the players, matches, setups, rounds, events, and tournament specified
		command: "Get"
		, structure: "Bracket"
		, populate: ["players", "matches", "matches.players", "matches.setup", "round", "round.event", "round.event.tournament", "round.event.tournament.setups"]
		, id: req.params.b_id
	}, function (json) {
		var t = new req.Class.Tournament(json.round.event.tournament);
		if(Director.Validate(t, req.query.key, req.params.t_id)) { //valid key submitted
			var availableSetups = [];
			var availableMatches = [];
			var startedMatches = [];
			var finishedMatches = [];
			var r = new req.Class.Round(json.round, req.Class.Bracket);
			var b = new r.BracketConstructor(json);
			var e = new req.Class.Event(json.round.event);

			json.round.event.tournament.setups.forEach(function(setup) { //partition setups by availability
				if(e.game == setup.game && !setup.friendlies && !setup.match) {
					availableSetups.push(setup);
				}
			});
			
			json.matches.forEach(function(match){ //partition matches by state
				if(match.state == "open") {
					availableMatches.push(match)
				}
				if(match.state == "active") {
					startedMatches.push(match)
				}
				if(match.state == "complete") {
					finishedMatches.push(match);
				}
			});
			
			if(r.bracketType == "RoundRobin")
			{
				startedMatches.forEach(function(startedMatch) {
					for(var i = availableMatches.length-1; i >= 0; i--)
					{
						if(   startedMatch.players[0]._id == availableMatches[i].players[0]._id 
						   || startedMatch.players[0]._id == availableMatches[i].players[1]._id
						   || startedMatch.players[1]._id == availableMatches[i].players[0]._id
						   || startedMatch.players[1]._id == availableMatches[i].players[1]._id)
						{
							availableMatches.splice(i, 1);
						}
					}
						
				});
			}
			
			res.render('manage-matchlist', { //render page
				round: r
				, availableMatches: availableMatches
				, availableSetups: availableSetups
				, startedMatches: startedMatches
				, finishedMatches: finishedMatches
				, bracket: json
				, customStylesheet: [
					, {css: 'matches.css'}
				]
				,  customJS: [
					, {js: 'pages/manage-matchlist.js'}
				]
			});
		} else { //invalid key submitted
			Director.accessDenied('/manage/' + req.params.t_id + "/" + req.params.b_id + '?key=', res);
		}
	}, Director.renderCallback("404", res));
});

//render management view for all setups for a given tournament
router.get('/:t_id/setups', function(req, res) {
	Director(req.Class.Database, { //get tournament, setups, matches, and players specified
		command:"Get"
		, structure: "Tournament"
		, populate: ["setups", "setups.match", "setups.match.players"]
		, id: req.params.t_id
	}, function(json) {
		var t = new req.Class.Tournament(json);
		if(Director.Validate(t, req.query.key, req.params.t_id)) { //valid key submitted
			res.render('manage-setups', { //render page
				tournament: json
				, customJS: [
					{js: 'pages/manage-setups.js'}
				]
			});
		} else { //invalid key submitted
			Director.accessDenied('/manage/' + req.params.t_id + '/setups?key=', res);
		}
	}, Director.renderCallback("500", res));
});

//render payout management page for a given tournament
router.get('/:t_id/payouts', function(req, res) {
	Director(req.Class.Database, { //get tournament and people specified
		command:"Get"
		, structure: "Tournament"
		, populate: ["registrants"]
		, id: req.params.t_id
	}, function(json) {
		var t = new req.Class.Tournament(json);
		if(Director.Validate(t, req.query.key, req.params.t_id)) { //valid key submitted
			var owes = [];
			var owed = [];
			json.registrants.forEach(function(reg) {
				if(reg.owed > 0)
					owes.push(reg)
				if(reg.owed < 0) {
					reg.owed *= -1;
					owed.push(reg)
				}
			});
			res.render('manage-payouts', { //render page
				tournament: json
				, owes: owes
				, owed: owed
				, customJS: [
					{js: 'pages/manage-payouts.js'}
				]
			});
		} else { //invalid key submitted
			Director.accessDenied('/manage/' + req.params.t_id + '/setups?key=', res);
		}
	}, Director.renderCallback("500", res));
});

//handle starting an event for a given event and tournament
router.post('/:t_id/e/:e_id', function(req, res) {
	if(req.body.command == "start") {
		Director(req.Class.Database, { //get tournament and event specified
			command: "Get"
			, structure: "Event"
			, populate: ["tournament"]
			, id: req.params.e_id
		}, function (json) {
			var t = new req.Class.Tournament(json.tournament);
			if(Director.Validate(t, req.query.key, req.params.t_id)) { //valid key submitted
				var e = new req.Class.Event(json);
				if(e.state == "pending") {
					req.Class.Challonge.StartEvent(req.params.e_id, req.Class, function(err, obj) { //start the event
						if(err) res.send(err);
						else res.send(obj);
					}, 1);
				}
			} else { //invalid key submitted
				Director.renderCallback("403", res)();
			}
		}, Director.renderCallback("404", res));
	}
});

//handle adding setups for a given tournament
router.post('/:t_id/s/:s_id/', function(req, res) {
	Director(req.Class.Database, { //get the tournament specified
		command: "Get"
		, structure: "Tournament"
		, id: req.params.t_id
	}, function(json) {
		var t = new req.Class.Tournament(json);
		if(Director.Validate(t, req.query.key, req.params.t_id)) { //valid key submitted
			Director(req.Class.Database, { //apply update to setups
				command: "Update"
				, structure: "Setup"
				, id: req.params.s_id
				, object: { 
					friendlies: req.body.friendlies
				}
			}, function(json) {
				res.send({});
			}, Director.renderCallback("500", res));
		} else { //invalid key submitted
			Director.renderCallback("403", res)();
		}
	}, Director.renderCallback("500", res));
});

//handle starting a match for a given tournament , event, and match
router.post('/:t_id/e/:e_id/startmatch', function(req, res){
	Director(req.Class.Database, { //get the specified tournament and event
		command: "Get"
		, structure: "Tournament"
		, id: req.params.t_id
	}, function (json) {
		var t = new req.Class.Tournament(json);
		if(Director.Validate(t, req.query.key, req.params.t_id)) { //valid key submitted
			Director(req.Class.Database, { //free the setup that the match occupied if relevant
				command: "Update"
				, structure: "Setup"
				, id: req.body.s_id
				, object: {
					match: req.body.m_id
				}
			}, function(obj) {
				Director(req.Class.Database, { //update the match sent and any othe relevant data
					command: "Update"
					, structure: "Match"
					, id: req.body.m_id
					, object: {
						state: "active"
						, setup: req.body.s_id
					}
				}, function(obj) {
					res.send(obj);
				}, Director.renderCallback("500", res))
			}, Director.renderCallback("500", res));
		} else { //invalid key submitted
			Director.renderCallback("403", res)();
		}
	}, Director.renderCallback("500", res));

});

module.exports = router;
