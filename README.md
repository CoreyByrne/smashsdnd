#TOryaa

### A one stop solution for running your smash tournament

## Links

* [Google Drive](https://drive.google.com/drive/u/0/folders/0BwDzN5Skh9SLVTVVN2gwTVIwYTg)
* [Bitbucket Repo](https://bitbucket.org/CoreyByrne/smashsdnd)
* [Milestone 2 Doc](https://docs.google.com/document/d/1WipmgQGQ6iZfYajAOW6j-f-Gvxx6uHT5948POIg2bLs/edit?usp=sharing)

## Getting Started
* Install NodeJS, NPM, and MongoDB (nodejs, npm, mongodb)
* Clone this repo
* Create a new folder ```data``` in the repo root
* Run ```npm install```
* Optionally run ```(sudo) npm install -g forever nodemon```
* Optionally add binaries to ```PATH```

## Running
* Run mongod from MongoDB through terminal with the option ```--dbpath=<path to database>```
* The database can be stored anywhere on your machine
* Run with either ```npm start``` or ```node bin/www``` in repo root
* Optionally run with ```forever``` and/or ```nodemon``` (see start and start.bat)

### Usage:
* Go to http://localhost:3000 in browser for index

### Important Files:
* app.js is where everything begins
* /routes/ is how the nodeJS server reroutes requests to the right views
* /views/ are all the views that the UI consist of
* /public/javascripts/class/ contains the major back-end modules (Match, Bracket, Challonge, Database, etc)
