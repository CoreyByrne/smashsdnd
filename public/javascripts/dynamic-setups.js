// this just handles dynamic form addition for setups
$(document).ready(function(){
	var next = 0;  
	$(".add-more").click(function(e){
		e.preventDefault();
		var addto = "#inputSetup" + next;
		var addRemove = "#inputSetup" + next;
		next = next + 1;
		var newIn = '<input type="text" class="form-control dynamic-setup" id="inputSetup' + next + '" name="setup' + next + '" placeholder="Super Smash Bros. for Wii U">';
		var newInput = $(newIn);
		var removeBtn = '<button id="remove' + (next - 1) + '" class="btn btn-danger remove-me dynamic-setup" >-</button><br>';
		var removeButton = $(removeBtn);
		$(addto).after(newInput);
		$(addRemove).after(removeButton);
		$("#inputSetup" + next).attr('data-source',$(addto).attr('data-source'));
		$("#count").val(next);
		$('.remove-me').click(function(e){
			e.preventDefault();
			var fieldNum = this.id.charAt(this.id.length-1);
			var fieldID = "#inputSetup" + fieldNum;
			$(this).remove();
			$(fieldID).remove();
		});
	});
});
