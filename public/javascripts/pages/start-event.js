$('#form').on('submit', function() {
	$.urlParam = function(name){
		var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
		return results[1] || 0;
	}

	// post data to server
	$.ajax({
		type:"POST"
		, url: "/manage/" + $("#inputId").attr('value')
				+ "?key=" + $.urlParam("key")
		, data: { command: "start" }
		, success: function(obj) {
			if(obj.err)
				alert(obj.err);
			else {
				window.location.replace(window.location.href + "&success=true");
			}
		}
		, dataType: 'json'
	});
});
