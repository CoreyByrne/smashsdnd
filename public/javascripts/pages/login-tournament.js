// validate a key to access tournament administration
document.addEventListener("DOMContentLoaded", function() {
	$('#inputSubmit').bind('click', function(){
		if($('#inputKey').val()) {
			var cbURL = $('#cbURL').val() + $('#inputKey').val();
			document.location.href = cbURL;
			return false;
		}
	});
});
