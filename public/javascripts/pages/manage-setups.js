$.urlParam = function(name){
	var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
	return results[1] || 0;
}
$(".setupStatus").on("change", function(e) {
	// get input values
	var s_id = $(e.target).parent().find('input.setupId').val();
	var state = $(e.target).find("option:selected").val();

	// post to server
	$.ajax({
		type:"POST"
		, url: "/manage/" + $("#inputId").val()
				+ "/s/" + s_id
				+ "?key=" + $.urlParam("key")
		, data: { friendlies: state }
		, success: function(obj) {
			window.location.replace(
				"/manage/" + $("#inputId").val()
				+ "/setups"
				+ "?key=" + $.urlParam("key")
				+ "&success=true"
				);
		}
		, dataType: 'json'
	});
});

// success alert
if($.urlParam("success") == "true") {
	$('#main').prepend('<div class="alert alert-success"><strong>Success!</strong> Setups have been changed.</div>');
}
