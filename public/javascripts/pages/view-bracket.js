// form on view-bracket specifically for submission of matches
$.urlParam = function(name){
	var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
	return results[1] || 0;
}

// Submits matches
// In-Progress -> Completed
$('.submitMatch').on('click', function(e) {
	var parent = $(this).parent().parent();

	// get values from modal dialog box
	var bracketId = $("#inputBId").val();
	var matchId = $(parent).find('input.match').val();
	var setupId = $(parent).find('input.setup').val();
	var playerScore1 = $(parent).find('input.player0').val();
	var playerScore2 = $(parent).find('input.player1').val();
	var playerId1 = $(parent).find('input.hidden0').val();
	var playerId2 = $(parent).find('input.hidden1').val();
	var winnerId, loserId;

	console.log(setupId);

	// check score values
	if(playerScore1 == "" || playerScore1 < 0
	|| playerScore2 == "" || playerScore2 < 0) {
			window.location.replace(
					"/view"
					+ "/b/" + $("#inputBId").val()
					+ "?success=false"
			);
			return;
	}

	// get winner id
	if(playerScore1 > playerScore2) {
		winnerId = playerId1;
		loserId = playerId2;
	} else {
		winnerId = playerId2;
		loserId = playerId1;
	}

	// store data for post
	var data = {
		m_id: matchId
		, s_id: setupId
		, b_id: bracketId
		, score: playerScore1 + "-" + playerScore2
		, winnerId: winnerId
		, loserId: loserId
	}

	// post to server
	$.ajax({
		type: "POST"
		, url: "/submit"
		, data: data
		, success: function loadNext(doc) {
			window.location.replace(
					"/view"
					+ "/b/" + $("#inputBId").val()
					+ "?success=true"
			);
		}
		, dataType: 'json'
	});

	// replace anyway - covers for the no setup feature
	window.location.replace(
			"/view"
			+ "/b/" + $("#inputBId").val()
			+ "?success=true"
	);
	return;
});

// success alert
if($.urlParam("success") == "true") {
	$('#main').prepend('<div class="alert alert-success"><strong>Success!</strong> Match has been submitted.</div>');
}

// failure alert
if($.urlParam("success") == "false") {
	$('#main').prepend('<div class="alert alert-danger"><strong>Failure:</strong> Match has not been submitted.</div>');
}
