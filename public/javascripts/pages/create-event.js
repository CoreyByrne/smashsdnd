$.urlParam = function(name){
	var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
	return results[1] || 0;
}
$('#form').on('submit', function(e){
	var payout = [];
	var total = 0;

	// get respective payouts
	$('.inputPayoutPercent').each(function(i, p) {
		payout.push(parseInt($(p).val()));
		total += payout[i];
	});

	// total payout and check if 100%
	if(total != 100) {
		alert("Payout total isn't 100%");
		console.log(total);
		return;
	}
	
	// create Event data to send via POST
	var e = new Event({
		name: $("#inputName").val()
		, game: $("#inputGame option:selected").val()
		, description: $("#inputDescription").val()
		, tournament: $("#inputId").attr('value')
		, potBonus: $("#inputPrizePool").val()
		, entranceFee: $("#inputEntryFee").val()
		, payoutSchema: payout
	});

	var data = e.CreateExport();

	data.roundData = [];

	// add round data for brackets
	$('.inputBrackets').each(function(i, b) {
		var roundData = {};
		roundData.bracketType = $(b).find('select option:selected').val();
		roundData.entrantsPerBracket = $(b).find('.entrants input').val();
		roundData.numberOfWinners = $(b).find('.winners input').val();
		data.roundData.push(roundData);
	});

	data.roundData[data.roundData.length - 1].entrantsPerBracket = 0;

	// post data
	$.ajax({
		type:"POST"
		, url: "/create/" + $("#inputId").attr('value')
						+ '?key=' + $.urlParam("key")
		, data: data
		, success: function(obj) {
			window.location.replace(
						"/create/" + $("#inputId").attr('value')
						+ '?key=' + $.urlParam("key") + "&success=true");
		}
		, dataType: 'json'
	});
});

// inputRounds dynamic form addition and removal
$('#inputRounds').on('change', function(e) {
	var r = $(e.target).val();
	if(r < 1) {
		$(e.target).val(1);
		r = 1;
	}
	if(r > 2) {
		$(e.target).val(2);
		r = 2;
	}
	var b = $('.inputBrackets');
	if(b.length == r)
		return;

	if(b.length < r) {
		var k = b.length;
		for(var i = 0; i < r - b.length; i++) {
			var o = b.last();
			var n = b.last().clone();
			var j = k + 1 + i;
			n.find('h3.round').text('Round ' + j);
			b.parent().append(n);
		}
		b = $('.inputBrackets');
		b.find('.entrants').show(200);
		b.find('.winners').show(200);
		b.last().find('.entrants').hide();
	} else {
		var i;
		for(i = b.length - 1; i >= r; i--) {
			b.eq(i).remove();
		}
		b.eq(i).find('.entrants').hide(200);
		b.eq(i).find('.winners').hide(200);
	}

	if(b.length == 2) {
		b.first().find('option').toggle();
		b.first().find('select').val('RoundRobin');
		b.last().find('.winners').hide(200);
	}
});

// inputPayout dynamic form addition and removal
var next = 0;
$(".add-more").click(function(e){
	e.preventDefault();
	var addto = "#payout" + next;
	++next;

	var ext;
	if(next <= 12 && next >= 10)
		ext = "th"
	else if(next % 10 == 0)
		ext = "st"
	else if(next % 10 == 1)
		ext = "nd"
	else if(next % 10 == 2)
		ext = "rd"
	else
		ext = "th"

	var n = '<div class="form-inline" id="payout' + next + '" class="payout"> \
		<label style="width:50px;">' + (next + 1) + ext + '</label> \
		<div class="input-group"> \
			<input type="number" min="0" data-number-to-fixed="2" class="form-control inputPayoutPercent" style="width:100px"> \
			<span class="input-group-addon">%</span> \
		</div> \
	</div>';

	// create our massive input form
	var newInput = $(n)

	$(addto).after(newInput);
});

// removal function
$('.remove-more').click(function(e){
	e.preventDefault();
	if(next > 0) {
		var fieldID = "#payout" + next;
		$(fieldID).remove();
		--next;
	}
});

// success alert
if($.urlParam("success") == "true") {
	$('#main').prepend('<div class="alert alert-success"><strong>Success!</strong> Event has been created.</div>');
}
