$('#form').on('submit', function(e){
	var games = [];
	var gameList = $('.game');

	for(var i = 0; i < gameList.length; i++) {
		games.push($(gameList[i]).val());
	}

	// store data
	var t = new Tournament({
		name:$('#inputName').val()
		, creator: $('#inputEmail').val()
		, key: $('#inputKey').val()
		, description: $('#inputDescription').val()
		, acceptSetups: $('#inputAcceptSetups').is(':checked')
		, games: games
		, location: $('#inputLocation').val()
		, date: $("#inputDate").val()
	});

	// post to create
	$.ajax({
		type:"POST"
		, url: "/create"
		, data: t.CreateExport(withKey = true)
		, success: loadNext
		, dataType: 'json'
	})
});

function loadNext(doc) {
	var t = new Tournament(doc);
	window.location.replace("/create/" + t._id + "?key=" + $('#inputKey').val() );
}

$(document).ready(function () {
	$('.datepicker').datepicker({
		format: "mm/dd/yyyy"
	});
});
