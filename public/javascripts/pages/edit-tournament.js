$.urlParam = function(name){
	var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
	return results[1] || 0;
}
$('#form').on('submit', function(e){

	var games = [];
	var gameList = $('.game');

	// get game list
	for(var i = 0; i < gameList.length; i++) {
		if($(gameList[i]).val() != "")
			games.push($(gameList[i]).val());
	}

	// store new games and setups
	var t = {
		name:$('#inputName').val()
		, description: $('#inputDescription').val()
		, acceptSetups: $('#inputAcceptSetups').is(':checked')
		, games: games
		, location: $('#inputLocation').val()
		, date: $("#inputDate").val()
	};

	// post to server
	$.ajax({
		type:"POST"
		, url: "/edit/" + $("#inputId").attr('value')
						+ '?key=' + $.urlParam("key")
		, data: t
		, success: function loadNext(doc) {
			window.location.replace(
					"/edit/" + $("#inputId").attr('value')
					+ '?key=' + $.urlParam("key")
					+ '&success=true');
		}
		, dataType: 'json'
	})
});

$(document).ready(function () {
	$('.datepicker').datepicker({
		format: "mm/dd/yyyy"
	});

	// success alert
	if($.urlParam("success") == "true") {
		$('#main').prepend('<div class="alert alert-success"><strong>Success!</strong> Tournament has been edited.</div>');
	}
});
