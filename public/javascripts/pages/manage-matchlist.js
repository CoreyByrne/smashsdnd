// function to grab url parameters
$.urlParam = function(name) {
	var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
	return results[1] || 0;
}

// Starts matches
// Available -> In-Progress
$('.startMatch').on('click', function(e) {
	var parent = $(this).parent();

	// get input values
	var matchId = $(parent).parent().find('input.match').val();
	var setupId = $(parent).find('select option:selected').val();

	// store data for post
	var data = {
		m_id: matchId
		, s_id: setupId
	}

	// post to server
	$.ajax({
		type: "POST"
		, url: "/manage/" + $("#inputTId").val() + "/e/" + $("#inputEId").val() + "/startmatch?key=" + $.urlParam('key')
		, data: data
		, success: function loadNext(doc) {
			window.location.replace(
					"/manage/" + $("#inputTId").val()
					+ "/b/" + $("#inputBId").val()
					+ "?key=" + $.urlParam('key')
					+ "&success=startTrue"
			);
		}
		, dataType: 'json'
	})
});

// Submits matches
// In-Progress -> Completed
$('.submitMatch').on('click', function(e) {
	var parent = $(this).parent();

	// get values from page
	var bracketId = $("#inputBId").val();
	var matchId = $(parent).children('input.match').val();
	var setupId = $(parent).children('input.setup').val();
	var player1 = $(parent).children('.tournamentMatch').children('.tournamentPlayer0');
	var player2 = $(parent).children('.tournamentMatch').children('.tournamentPlayer1');
	var playerScore1 = $(player1).children('input.player0').val();
	var playerScore2 = $(player2).children('input.player1').val();
	var playerId1 = $(player1).children('input.hidden0').val();
	var playerId2 = $(player2).children('input.hidden1').val();
	var winnerId, loserId;

	// check score values
	if(playerScore1 == "" || playerScore1 < 0
	|| playerScore2 == "" || playerScore2 < 0) {
			window.location.replace(
					"/manage/" + $("#inputTId").val()
					+ "/b/" + $("#inputBId").val()
					+ "?key=" + $.urlParam('key')
					+ "&success=submitFalse"
			);
			return;
	}

	// get winner id
	if(playerScore1 > playerScore2) {
		winnerId = playerId1;
		loserId = playerId2;
	} else {
		winnerId = playerId2;
		loserId = playerId1;
	}

	// store data for post
	var data = {
		m_id: matchId
		, s_id: setupId
		, b_id: bracketId
		, score: playerScore1 + "-" + playerScore2
		, winnerId: winnerId
		, loserId: loserId
	}

	// post to server
	$.ajax({
		type: "POST"
		, url: "/submit"
		, data: data
		, success: function loadNext(doc) {
			window.location.replace(
					"/manage/" + $("#inputTId").val()
					+ "/b/" + $("#inputBId").val()
					+ "?key=" + $.urlParam('key')
					+ "&success=submitTrue"
			);
		}
		, dataType: 'json'
	})
});

// Resubmits matches
// Completed -> Completed
$('.editMatch').on('click', function(e) {
	var parent = $(this).parent();

	// get values from page
	var bracketId = $("#inputBId").val();
	var matchId = $(parent).children('input.match').val();
	var setupId = $(parent).children('input.setup').val();
	var player1 = $(parent).children('.tournamentMatch').children('.tournamentPlayer0');
	var player2 = $(parent).children('.tournamentMatch').children('.tournamentPlayer1');
	var playerScore1 = $(player1).children('input.player0').val();
	var playerScore2 = $(player2).children('input.player1').val();
	var playerId1 = $(player1).children('input.hidden0').val();
	var playerId2 = $(player2).children('input.hidden1').val();
	var winnerId, loserId;

	// check score values
	if(playerScore1 == "" || playerScore1 < 0
	|| playerScore2 == "" || playerScore2 < 0) {
			window.location.replace(
					"/manage/" + $("#inputTId").val()
					+ "/b/" + $("#inputBId").val()
					+ "?key=" + $.urlParam('key')
					+ "&success=resubmitFalse"
			);
			return;
	}

	// get the winner id
	if(playerScore1 > playerScore2) {
		winnerId = playerId1;
		loserId = playerId2;
	} else {
		winnerId = playerId2;
		loserId = playerId1;
	}

	// store data for post
	var data = {
		m_id: matchId
		, s_id: setupId
		, b_id: bracketId
		, score: playerScore1 + "-" + playerScore2
		, winnerId: winnerId
		, loserId: loserId
	}

	// post to submit
	$.ajax({
		type: "POST"
		, url: "/submit" + "?key=" + $.urlParam("key")
		, data: data
		, success: function loadNext(doc) {
			window.location.replace(
					"/manage/" + $("#inputTId").val()
					+ "/b/" + $("#inputBId").val()
					+ "?key=" + $.urlParam('key')
					+ "&success=resubmitTrue"
			);
		}
		, dataType: 'json'
	})
});

// success alerts
// startTrue: match starting success
// submitTrue: match submission success
// resubmitTrue: match resubmission success
if($.urlParam("success") == "startTrue") {
	$('#main').prepend('<div class="alert alert-success"><strong>Success!</strong> Match has been started.</div>');
}
if($.urlParam("success") == "submitTrue") {
	$('#main').prepend('<div class="alert alert-success"><strong>Success!</strong> Match has been submitted.</div>');
}
if($.urlParam("success") == "resubmitTrue") {
	$('#main').prepend('<div class="alert alert-success"><strong>Success!</strong> Match has been resubmitted.</div>');
}


// failure alerts
// submitFalse: match submission false
// resubmitFalse: match resubmission false
if($.urlParam("success") == "submitFalse") {
	$('#main').prepend('<div class="alert alert-danger"><strong>Failure:</strong> Match has been not submitted.</div>');
}
if($.urlParam("success") == "resubmitFalse") {
	$('#main').prepend('<div class="alert alert-danger"><strong>Failure:</strong> Match has been not resubmitted.</div>');
}
