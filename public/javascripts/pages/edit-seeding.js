$.urlParam = function(name){
	var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
	return results[1] || 0;
}
$('#form').on('submit', function(e) {
	var seeds = [];
	var seedList = $('.seed');

	// grab seeds
	for(var i = 0; i < seedList.length; i++) {
		seeds.push($(seedList[i]).attr('id'));
	}

	// store seeds
	var t = {
		seeds: seeds
	};

	// post to server
	$.ajax({
		type:"POST"
		, url: "/edit/" + $("#inputTId").attr('value')
						+ "/" + $("#inputEId").attr('value')
						+ "/seeding"
						+ '?key=' + $.urlParam("key")
		, data: t
		, success: function loadNext(doc) {
			window.location.replace(
						"/edit/" + $("#inputTId").attr('value')
						+ "/" + $("#inputEId").attr('value')
						+ "/seeding"
						+ '?key=' + $.urlParam("key")
						+ '&success=true'
					);
		}
		, dataType: 'json'
	})
});

$(function() {
	$("ul.seeds").sortable();
});

if($.urlParam("success") == "true") {
	$('#main').prepend('<div class="alert alert-success"><strong>Success!</strong> Seeds have been changed.</div>');
}
