$.urlParam = function(name){
	var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
	return results[1] || 0;
}
$('#form').on('submit', function(e) {
	var payout = [];
	var total = 0;

	// get payouts and add to total
	$('.inputPayoutPercent').each(function(i, p) {
		payout.push(parseInt($(p).val()));
		total += payout[i];
	});

	// check payouts
	// if(total != 100) {
	// 	alert("Payout total isn't 100%");
	// 	console.log(total);
	// 	return;
	// }

	// store edited values into data
	var data = {
		name: $("#inputName").val()
		, game: $("#inputGame option:selected").val()
		, description: $("#inputDescription").val()
		, tournament: $("#inputId").attr('value')
		, potBonus: $("#inputPrizePool").val()
		, entranceFee: $("#inputEntryFee").val()
	};

	// post data
	$.ajax({
		type:"POST"
		, url: "/edit/" + $("#inputTId").attr('value')
						+ "/" + $("#inputEId").attr('value')
						+ '?key=' + $.urlParam("key")
		, data: data
		, success: function(obj) {
			window.location.replace(
					"/edit/" + $('#inputTId').attr('value')
					+ "/" + $("#inputEId").attr('value')
					+ '?key=' + $.urlParam("key")
					+ '&success=true');
		}
		, dataType: 'json'
	});
});

// success alert
if($.urlParam("success") == "true") {
	$('#main').prepend('<div class="alert alert-success"><strong>Success!</strong> Event has been edited.</div>');
}
