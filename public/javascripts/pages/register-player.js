$.urlParam = function(name) {
	var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
	return results[1] || 0;
}

$('#form').on('submit', function(e){
	var events = [];
	var owed = 0;

	// get owed payouts
	$.each($('.event'), function(i, v) {
		var b = $(v).find(".id");
		if($(b).is(':checked')) {
			events.push(b.val());
			owed += parseInt($(v).find('.fee').val());
		}
	});

	// store Person for POSTing
	var p = new Person({
		firstName: $("#inputFirstName").val()
		, lastName: $("#inputLastName").val()
		, sponsor: $("#inputPrefix").val()
		, tag: $("#inputTag").val()
		, email: $("#inputEmail").val()
		, tournament: $("#inputId").attr('value')
		, events: events
		, owed: owed
	});

	var send = p.CreateExport();
	send.setups = [];

	// store setups for POSTing
	if($('#setup') && $('#setup').is(':checked')) {
		$('.input-game option:selected').each(function(i, game) {
			send.setups.push($(game).val());
		});
	}

	$.ajax({
		type:"POST"
		, url: "/register/" + $("#inputId").val()
		, data: send
		, success: function(obj) {
			window.location.replace(
					"/register/" + $("#inputId").val() + "?success=true");
		}
		, dataType: 'json'
	});
});

$("#setup").bind('click', function(){
	$("#setupList").slideToggle(100);
});

if($.urlParam("success") == "true") {
	$('#main').prepend('<div class="alert alert-success"><strong>Success!</strong> Player has been registered.</div>');
}
