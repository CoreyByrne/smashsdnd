$.urlParam = function(name){
	var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
	return results[1] || 0;
}

// success alert
if($.urlParam("success") == "true") {
	$('#main').prepend('<div class="alert alert-success"><strong>Success!</strong> Event has started.</div>');
}
