/*
 * This file contains the Bracket class, and its subclasses
 */

//abstract constructor
Bracket = 
{
	matches: [] //matches in bracket
	, players: [] //all players in bracket
	, complete: false //bracket completed; no more specific information needed
	, round: null //round that owns this bracket
	, bracketType: null //type of bracket
	, challongeID: 1 //ID of the challonge tournament used to create this bracket
	, numberOfWinners: 1 //number of players that make it out of this bracket
	, __proto__: {} //prototype needs to exist in order to make models
	//creates an object containing the minimum information needed to reconstruct this object from its own constructor
	//used for database storage and sometimes when passing objects
	, CreateExport: function ()
	{
		expo = {};
		expo.bracketType = this.bracketType;
		expo.complete = this.complete;
		expo.challongeID = this.challongeID;
		expo.round = this.round;

		expo.players = []; //may contain either Persons or PersonExports
		for(var p in this.players)
		{
			expo.players.push(p);
		}
		expo.matches = []; //may contain either Matches or MatchExports
		for(var m in this.matches)
		{
			expo.matches.push(m);
		}
		expo.numberOfWinners = this.numberOfWinners;
		
		if(this._id) expo._id = this._id;
		
		return expo;
	}
}

//constructor for concrete version of Bracket
function SingleElimBracket(BracketExport)
{
	this.bracketType = "single elimination";
	if(BracketExport.complete) this.complete = BracketExport.complete;
	if(BracketExport.challongeID) this.challongeID = BracketExport.challongeID;
	if(BracketExport.numberOfWinners) this.numberOfWinners = BracketExport.numberOfWinners;
	if(typeof BracketExport.round === 'undefined')
		console.log("Brackets must be declared with an owner!");
	this.round = BracketExport.round;
	
	if(BracketExport.matches) {
		matches = [];
		BracketExport.matches.forEach(function(match) {
			matches.push(match);
		});
		this.matches = matches;
	}
}

SingleElimBracket.prototype = //causes inheritance of Bracket default values
{
	__proto__: Bracket
	, rank: function() {
		// single elim is max round first
		roundRanking = [];
		this.matches.forEach(function(m) {
			if(!m.scoresCsv || m.scoresCsv.length == 0)
				return;
			var matchResult = m.scoresCsv.split('-');
			matchResult[0] = parseInt(matchResult[0]);
			matchResult[1] = parseInt(matchResult[1]);
			
			var loser;
			
			// we only need loser
			if(matchResult[0] > matchResult[1]) {
				loser = m.players[1]
			} else {
				loser = m.players[0]
			}
			
			var set = false;
			for(var i = 0; i < ranking.length; i++) {
				if(roundRanking[i] < m.round) {
					set = true;
					ranking.splice(i, 0, loser);
					roundRanking.splice(i, 0, m.round);
					break;
				}
			}
			if(!set) {
				roundRanking.push(m.round);
				ranking.push(loser);
			}
		});
		
		// add the one who never lost
		this.players.forEach(function(p) {
			if(ranking.indexOf(p) == -1) {
				ranking.splice(0, 0, p);
			}
		});
	}
}

//constructor for concrete version of Bracket
function DoubleElimBracket(BracketExport)
{
	this.bracketType = "double elimination";
	if(BracketExport.complete) this.complete = BracketExport.complete;
	if(BracketExport.challongeID) this.challongeID = BracketExport.challongeID;
	if(BracketExport.numberOfWinners) this.numberOfWinners = BracketExport.numberOfWinners;
	if(typeof BracketExport.round === 'undefined')
		console.log("Brackets must be declared with an owner!");
	this.round = BracketExport.round;
	
	if(BracketExport.matches) {
		matches = [];
		BracketExport.matches.forEach(function(match) {
			matches.push(match);
		});
		this.matches = matches;
	}
}

DoubleElimBracket.prototype = //causes inheritance of Bracket default values
{
	__proto__: Bracket
	, rank: function() {
		// other than first and second, its just lower round places higher
		ranking = [];
		var maxRound = 0;
		var maxRoundLoser;
		var maxRoundWinner;
		var roundRanking = [];
		this.matches.forEach(function(m) {
			if(!m.scoresCsv || m.scoresCsv.length == 0)
				return;
			var matchResult = m.scoresCsv.split('-');
			matchResult[0] = parseInt(matchResult[0]);
			matchResult[1] = parseInt(matchResult[1]);
			
			// keep track of winner and loser
			var loser, winner;
			if(matchResult[0] > matchResult[1]) {
				winner = m.players[0];
				loser = m.players[1];
			} else {
				winner = m.players[1];
				loser = m.players[0];
			}
			
			// if its a new maximum
			if(m.round >= maxRound) { // has to be equals, because reset grandfinals will be on same level as nonreset
				maxRoundWinner = winner;
				maxRoundLoser = loser;
				maxRound = m.round
			} 
			
			// otherwise its a loser round
			if(m.round < 0) {
				var set = false;
				for(var i = 0; i < ranking.length; i++) {
					if(roundRanking[i] > m.round) {
						ranking.splice(i, 0, loser);
						roundRanking.splice(i, 0, m.round);
						set = true;
						break;
					}
				}
				if(!set) {
					roundRanking.push(m.round);
					ranking.push(loser);
				}
			}
		});
		
		// add the grand finals
		ranking.splice(0, 0, maxRoundLoser);
		ranking.splice(0, 0, maxRoundWinner);
		return ranking;
	}
}

//constructor for concrete version of Bracket
function RoundRobin(BracketExport)
{
	this.bracketType = "round robin";
	if(BracketExport.complete) this.complete = BracketExport.complete;
	if(BracketExport.challongeID) this.challongeID = BracketExport.challongeID;
	if(BracketExport.numberOfWinners) this.numberOfWinners = BracketExport.numberOfWinners;
	if(typeof BracketExport.round === 'undefined')
		console.log("Brackets must be declared with an owner!");
	this.round = BracketExport.round;
	
	if(BracketExport.matches) {
		matches = [];
		BracketExport.matches.forEach(function(match) {
			matches.push(match);
		});
		this.matches = matches;
	}
}

RoundRobin.prototype = //causes inheritance of Bracket default values
{
	__proto__: Bracket
	, rank: function () {
		var score = {};
		var ranking = [];
		this.players.forEach(function(p) {
			scores[p] = {
				setCount: 0
				, gameCount: 0
			};
		});
		
		// for each match put in the score
		this.matches.forEach(function(m) {
			if(!m.scoresCsv || m.scoresCsv.length == 0)
				return;
			var matchResult = m.scoresCsv.split('-');
			matchResult[0] = parseInt(matchResult[0]);
			matchResult[1] = parseInt(matchResult[1]);
			
			if(matchResult[0] > matchResult[1]) {
				scores[m.players[0]].setCount += 1;
			} else if (matchResult[0] < matchResult[1]) {
				scores[m.players[1]].setCount += 1;
			}
			
			scores[m.players[0]].gameCount += matchResult[0] - matchResult[1];
			scores[m.players[1]].gameCount += matchResult[1] - matchResult[0];
		});
		
		// for each player, rank by score
		this.players.forEach(function(p) {
			var added = false;
			for(var i = 0; i < ranking.length; i++) {
				if(scores[ranking[i]].setCount < scores[p].setCount 
				|| (scores[ranking[i]].setCount == scores[p].setCount
					&& scores[ranking[i]].gameCount < scores[p].gameCount)) {
					ranking.splice(i, 0, p);
					added = true;
					break;
				}
			}
			if(!added)
				ranking.splice(i, 0, p);
		});
		
		return ranking;
	}
}

if(typeof module !== 'undefined') //allows frontend to access this class
{
	module.exports.Bracket = Bracket;
	module.exports.DoubleElimBracket = DoubleElimBracket;
	module.exports.SingleElimBracket = SingleElimBracket;
	module.exports.RoundRobin = RoundRobin;
}