/*
 * This file contains the Person class
 */

// Constructor
function Person(PersonExport) 
{
	if(PersonExport.firstName) this.firstName = PersonExport.firstName; //first name of person
	if(PersonExport.lastName) this.lastName = PersonExport.lastName; //last name of person
	if(PersonExport.sponsor) this.sponsor = PersonExport.sponsor; //sponsor in person's tag
	if(PersonExport.tag) this.tag = PersonExport.tag; //person's tag
	if(PersonExport.email) this.email = PersonExport.email; //person's e-mail for password recovery
	if(PersonExport.tournament) this.tournament = PersonExport.tournament; //tournament this person registered for
	if(PersonExport.events) this.events = PersonExport.events; //events this person entered
	if(PersonExport.owed) this.owed = PersonExport.owed //money owed BY this person
	if(PersonExport._id) this._id = PersonExport._id; //database ID for this person
	
	return this;
}

//Body
Person.prototype = 
{
	//set default values for this object
	//however, these values MAY NOT BE HANDLED CORRECTLY by other functions
	firstName: "Missing"
	, lastName: "No."
	, sponsor: ""
	, tag: "Missing"
	, email: "Missing"
	, events: []
	, owed: 0
	
	//creates an object containing the minimum information needed to reconstruct this object from its own constructor
	//used for database storage and sometimes when passing objects
	, CreateExport: function ()
	{
		expo = {};
		expo.firstName = this.firstName;
		expo.lastName = this.lastName;
		expo.sponsor = this.sponsor;
		expo.tag = this.tag;
		expo.email = this.email;
		expo.tournament = this.tournament;
		expo.owed = this.owed;
		if(this._id) expo._id = this._id;
		
		expo.events = []
		this.events.forEach(function(event) { //may contain either Events or EventExports
			expo.events.push(event)
		});
		
		return expo;
	}
}

if(typeof module !== 'undefined') //allows frontend to access this class
{
	module.exports = Person;
}