/*
 * This file contains the Round class
 */

//Constructor
function Round(RoundExport, BracketClass)
{
	if(RoundExport.number) this.number = RoundExport.number; //round number in event
	if(typeof RoundExport.event === 'undefined')
		console.log("Rounds must be declared with an owner!");
	this.event = RoundExport.event; //event that owns this round
	this.bracketType = RoundExport.bracketType; //type of bracket contained in this round
	
	if(RoundExport.brackets) { //brackets contained in this round
		var brackets = [];
		RoundExport.brackets.forEach(function(bracket) {
			brackets.push(bracket);
		});
		this.brackets = brackets;
	}
	
	if(RoundExport.entrantsPerBracket) this.entrantsPerBracket = RoundExport.entrantsPerBracket; //maximum entrants per bracket for this round (optinal)
	if(RoundExport.numberOfWinners) this.numberOfWinners = RoundExport.numberOfWinners;
		
	if(RoundExport.seeding) { //people in this round in seeded order; may or may not contain exports
		var seeding = [];
		RoundExport.seeding.forEach(function(seed) {
			seeding.push(seed);
		});
		this.seeding = seeding;
	}
	
	this.BracketConstructor = eval("BracketClass." + this.bracketType); //constructor for bracket specified by bracketType
	
	//conversion between toryaa bracket types and challonge bracket types
	if (RoundExport.bracketType == "SingleElimBracket") this.challongeType = "single elimination";
	else if (RoundExport.bracketType == "DoubleElimBracket") this.challongeType = "double elimination";
	else if (RoundExport.bracketType == "RoundRobin") this.challongeType = "round robin";
	else console.log("what kind of event are you even trying to run, buddy?");

	return this;
}

//Body
Round.prototype = 
{
	//set default values for this object
	//however, these values MAY NOT BE HANDLED CORRECTLY by other functions handling Tournament
	number: -1
	, event: {}
	, brackets: []
	, bracketType: "DoubleElimBracket"
	, challongeType: "double elimination"
	, entrantsPerBracket: -1
	, numberOfWinners: 2
	, complete: false
	
	// A function reference to the correct constructor
	, BracketConstructor: function() {}
	
	//creates an object containing the minimum information needed to reconstruct this object from its own constructor
	//used for database storage and sometimes when passing objects
	, CreateExport: function (exportBrackets)
	{
		var expo = {};
		expo.number = this.number;
		expo.event = this.event;
		expo.bracketType = this.bracketType;
		expo.challongeType = this.challongeType;		
		expo.entrantsPerBracket = this.entrantsPerBracket;
		expo.numberOfWinners = this.numberOfWinners;
		expo.brackets = [];
		expo.complete = this.complete;

		this.brackets.forEach(function(bracket) { //may contain either Brackets or BracketExports
			expo.brackets.push(bracket);
		});
		if(this._id) expo._id = this._id;
		
		return expo;
	}
}

if(typeof module !== 'undefined') //allows frontend to access this class
{
	module.exports = Round;
}