/*
	Setups are places where matches can be played.

	In Smash Tournaments, Setups usually refer to a TV-console pairing, but in a soccer tournament
	a setup may refer to a pitch. A tennis tournament setup may refer to a court, etc.
*/

function Setup(SetupExport) 
{
	if(SetupExport.owner) this.owner = SetupExport.owner; // personID who owns a setup
	if(SetupExport.friendlies) this.friendlies = SetupExport.friendlies; // whether setup can be used for tournament or not (friendlies setup = no)
	if(SetupExport.tournament) this.tournament = SetupExport.tournament; // tournaments own a setup
	if(SetupExport.match) this.match = SetupExport.match; // matches can be played on a setup (setups with matches shold not be assignable)
	if(SetupExport.game) this.game = SetupExport.game; // setups are of a specific game
	if(SetupExport.name) this.name = SetupExport.name; // setups can have names (i.e. "billy bob joe")
	if(SetupExport._id) this._id = SetupExport._id;
	return this; 
}

Setup.prototype = 
{
	owner: "None"
	, tournament: {}
	, friendlies: false
	, game: ""
	, name: ""
	, CreateExport: function () // Setup is an Exportable class and thus has a CreateExport function
	{
		expo = {};
		expo.owner = this.owner;
		expo.friendlies = this.friendlies;
		expo.tournament = this.tournament;
		expo.match = this.match;
		expo.game = this.game;
		expo.name = this.name;
		if(this._id) expo._id = this._id;
		return expo;
	}
}

if(typeof module !== 'undefined')
{
	module.exports = Setup;
}