// Database handling object
// @view <-- control f to see questions that need attention

/*
	these are boiled down to:
	gets
	updates
	adds

	see the main add, update and get functions, those are crucial to the running of this program
	also the idea of populates is very key, and is crucial to using mongo with this program because
	of the class structure
*/

function Database(mongoose, client) {
	Database.mongoose = mongoose;
}

// called in app.js to model new objects for mongo (only worry about this if adding new objects, not likely)
Database.AddSchemaAndModel = function(name, model, Schema)
{
	models[name] = model;
	schemas[name] = Schema;
}

// convenience
Database.SetClass = function(Class_) {
	Class = Class_;
}

// No need for more than one database object, so no need to create a prototype
models = {};
schemas = {};
Class = {};

// calls callback on all tournaments, populates as needed
Database.GetTournaments = function(callback, populates) {
	var q = models.Tournament.find({});
	if(typeof populates !== 'undefined') {
		q.deepPopulate(populates);
	}
	q.exec(callback);
}

/*
	CHECKS IF SOMETHING IS COMPLETE:
	pass in a token, it will figure out what to check and how to check it
	the callback will return true or false depending on whether it is complete or not.
*/
Database.CheckCompletion = function(token, callback, id) {
	var stack = [];
	stack['Bracket'] = {item: 'Match', query: {bracket: id, state: {$in: ["active", "pending", "open"]}}};
	stack['Round'] = {item: 'Bracket', query: {round: id, complete: false}};
	stack['Event'] = {item: 'Round', query: {event: id, complete: false}};
	stack['Tournament'] = {item: 'Event', query: {tournament: id, state: {$in: ["active", "pending", "open"]}}};

	this.GetQuery(stack[token].item, function(err, data) {
		if (err) {console.log(err);}
		if (data == null) callback(null, true);
		else callback(null, false);
	}, stack[token].query);
}

// specific get call for getting a round from an event, sample usage in challonge.startevent()
Database.GetNthRoundOfEvent = function(callback, e_id, roundNum) {
	console.log("searching for " + roundNum + " in " + e_id);
	var q = models.Round.findOne({ event: e_id, number: roundNum });
	q.exec(callback);
}

/*
	SPECIFIC GET FUNCTION, FOR MOST GETS USE DATABASE.GET (BELOW)
	if you need a custom search, for instance (finding a player by challongeID), use this
*/
Database.GetQuery = function(token, callback, find, populate) {
	var q = models[token].findOne(find);
	if(typeof populate !== 'undefined') {
		q.deepPopulate(populate);
	}
	q.exec(callback);
}

/*
	DECENTLY UNIVERSAL GET FUNCTION

	Need something from the database pretty generally, use this!
	Examples of how to use this are scattered throughout the code.

	Token should be of the form 'Match', 'Event', etc.
	Callback is called on the data coming back (if it doesn't exist, data will be null but err will be null too if call is formed correctly)
	Id is id of what to find
	Populate is an array of fields (i.e. 'event', 'events', 'events.rounds', etc.). These fields should be present in the object(s) requested
	from the database, and by default correspond to arrays containing ObjectIds; this command will replace each of those ObjectIds with the 
	objects they reference in the database. 
*/
Database.Get = function(token, callback, id, populate) {
	var q = models[token].findOne({ _id: id });
	if(typeof populate !== 'undefined') {
		q.deepPopulate(populate);
	}
	q.exec(callback);
}

/*
	DECENTLY UNIVERSAL UPDATE FUNCTION

	Need to update something (set) from the database pretty generally, use this!
	Setting a value overwrites the existing value for the given entry in the database. 
	Examples of how to use this are scattered throughout the code.

	Token should be of the form 'Match', 'Event', etc.
	Callback is called on the data coming back (if it doesn't exist, data will be null but err will be null too if call is formed correctly)
	Id is id of what to find
	O is what to set (if needing a push, use updatePush)
	Extra is an auxiliary field, for use in overrides of this function if necessary
*/
Database.Update = function(token, callback, id, o, extra) {
	if(this["Update" + token])
		this["Update" + token](callback, id, o, extra);
	else {
		if(o.CreateExport)
			models[token].update({ _id: id }, { $set: o.CreateExport() }, callback);
		else
			models[token].update({ _id: id }, { $set: o }, callback);
	}
}

/*
	DECENTLY UNIVERSAL UPDATE FUNCTION

	Need to update something (push) from the database pretty generally, use this!
	Pushing a value adds it to an array of values in the given entry in the database. 
	Examples of how to use this are scattered throughout the code.

	Token should be of the form 'Match', 'Event', etc.
	Callback is called on the data coming back (if it doesn't exist, data will be null but err will be null too if call is formed correctly)
	Id is id of what to find
	O is what to set (if needing a set, use update)
	Extra is an auxiliary field, for use in overrides of this function if necessary
*/
Database.UpdatePush = function(token, callback, id, o, extra) {
	if(this["UpdatePush" + token])
		this["UpdatePush" + token](callback, id, o, extra);
	else {
		if(o.CreateExport)
			models[token].update({ _id: id }, { $push: o.CreateExport() }, callback);
		else
			models[token].update({ _id: id }, { $push: o }, callback);
	}
}

/*
	DECENTLY UNIVERSAL ADD FUNCTION

	Need to add something to the database pretty generally, use this!
	Examples of how to use this are scattered throughout the code.

	Token should be of the form 'Match', 'Event', etc.
	Callback is called on the data coming back (if it doesn't exist, data will be null but err will be null too if call is formed correctly)
	Id is id of PARENT TO ADD TO (i.e. for a match, put the bracket id that contains it here)
	O is the actual object
	Extra is an auxiliary field, for use in overrides of this function if necessary
*/
Database.Add = function(token, callback, id, o, extra) {
	if(this["Add" + token])
		this["Add" + token](o, callback, id, extra);
	else {
		// default
		var chainOrder = [
			"Tournament", "Event", "Round"
			, "Bracket" , "Match"
		];

		var pushCommand = {
			"Tournament":"events"
			, "Event":"rounds"
			, "Round":"brackets"
			, "Bracket":"matches"
			, "Match":""
		}
		var ob = o.model();
		o._id = ob._id;

		var i = chainOrder.indexOf(token) - 1;
		if(i < 0) {
			ob.save(callback);
			return
		}

		push = {};
		push[pushCommand[chainOrder[i]]] = ob._id;

		models[chainOrder[i]].update( {_id: id}, {
			$push: push
		}, function(err, obj){
			if(err) {
				callback(err, obj);
				return;
			}

			ob.save(callback);
		});
	}
}

Database.AddEvent = function(event, callback, t_id, roundData) {
	var e = event.model();
	event._id = e._id;
	
	// add event to tournament
	models.Tournament.update({_id:t_id}
	, { 
		$push: {
			events:e._id
		}
	}
	, function(err, obj) {
		if (err) console.log(err);
		// save the event to the database
		e.save(function() {
			// when we finished, we need to add the rounds
			Database.AddRounds(roundData, Class.Round, Class.Bracket, e._id, function() {
				callback(err, obj);
			});
		});	
	})
	
}

// Override of Database.Add()
// generates a bracket using challonge, then copies the resulting bracket into the database
Database.AddBracket = function(bracket, callback, Class) {
	var b = bracket.model();
	bracket._id = b._id;

	Class.Challonge.CreateBracket(b,
		function(err, data){
	    	models.Round.update( {_id: bracket.round}, {
				$push: {brackets: b._id}
			}
			, function(err, obj){
				if(err) {
					console.log(err);
					callback(err, obj);
					return;
				}
				b.save(callback);
			});
	    }
	);
}

// updates the event once it has completed so that seeding contains the players to move on to the next round (in order)
Database.RankPlayers = function(b_id, callback) {
	Database.Get("Bracket", function(err, br) {
		console.log(Class);
		var r = new Class.Round(br.round, Class.Bracket);
		var b = new r.BracketConstructor(br);
		var ranking = b.rank();
	
		models.Event.update({_id: b.round.event}, {
			$set: {
				seeding: ranking
			}
		}, function(){
			callback(err, b);
		});
	}, b_id, ["round", "matches"]);
}

//Gives payouts for an event that has ended
Database.GivePayout = function(e_id, callback) {
	// get the event
	Database.Get("Event", function(err, e) {
		console.log(e)
		var payoutTotal = e.potBonus + e.entranceFee * e.seeding.length;
		var saves = e.payoutSchema.length;
		
		// for each of the payout schemas
		e.payoutSchema.forEach(function(p, i) {
			// "pay" the person
			if( e.seeding[i]) {
				models["Person"].update({_id: e.seeding[i]._id},{
					$set: {
						owed: e.seeding[i].owed - ((p / 100.) * payoutTotal)
					}
				}, function() {
					saves--;
					//when we're done
					if(saves == 0)
						callback();
				});
			} else {
				saves--;
				//when we're done
				if(saves == 0)
					callback();
			}
		});
	}, e_id, ["seeding"]);
}

// override of Database.Add()
// adds a person to the database, has its own function because people are owned by tournaments and events
Database.AddPerson = function(player, callback, t_id, events) {
	var p = player.model();
	player._id = p._id;

	var saves = events ? events.length + 1 : 1
	
	// add them to tournament
	models.Tournament.update( {_id: t_id}, {
		$push: {
			registrants: p._id
		}
	}, function(err, obj){
		if(err) {
			callback(err, obj);
			return;
		}
		saves --;
		if(saves == 0)
			p.save(callback); // save people to db
	});

	if(events) {
		// add them to each event they entered
		events.forEach(function(e_id) {
			models.Event.update( {_id: e_id}, {
				$push: {
					seeding: p._id
				}
			}, function(err, obj){
				if(err) {
					callback(err, obj);
					return;
				}
				saves --;
				if(saves == 0)
					p.save(callback); // save people to db
			});
		});
	}
}

// override of Database.Add()
// adds a setup to mongo, needs its own because people and tournaments own setups
Database.AddSetup = function(setup, callback, t_id) {
	var s = setup.model();
	setup._id = s._id;
	models.Tournament.update( {_id: t_id}, {
		$push: {
			setups: s._id
		}
	}
	, function(err, obj){
		if(err) {
			callback(err, obj);
			return;
		}
		s.save(callback); // save setup to db
	});
}

// NOT an override of Database.Add()
// adds a person to the bracket, also adds in challonge (person to specific bracket online)
Database.AddPersonToBracket = function(b_id, p_id, callback, Class) {
	Class.Challonge.AddPersonToBracket(b_id, p_id, function(err, data){
		if (err) {console.log(err);}
		models.Bracket.update({_id: b_id},
		{
			$push: {players: p_id}
		}).exec(function(err,data) {if (err) {console.log(err);} });
		callback(err, data);
	});
}

// NOT an override of Database.Add()
// adds multiple rounds to an event, an example of a 2-round tournament is pools that feed into bracket
// rounds are pre-numbered and pre-linked; no race conditions exist
Database.AddRounds = function(rounds, Round, Brackets, e_id, callback) {
	var saves = rounds.length;
	rounds.forEach(function(roundData, i) {
		var r = new Round({
			bracketType: roundData.bracketType
			, entrantsPerBracket: roundData.entrantsPerBracket
			, numberOfWinners: roundData.numberOfWinners
			, event: e_id
			, number: i + 1
		}, Brackets);

		Database.Add("Round", function(err) {
			saves--;
			if(saves == 0)
				callback();
		}, e_id, r);
	});
}

// NOT an override of Database.Add()
// adds multiple setups to a tournament
// lock is for race conditions; it acts as a mutex, forcing the setups to be enumerated uniquely and each added once
var countDebug = 0;
var countLock = false;
var countQueue = [];
Database.AddSetups = function(setups, Setup, p_id, t_id, callback) {	
	countDebug ++;
	
	// mutex for "one at a time"
	function bind(done) {
		// the exec is my task
		return function exec() {
			// number of games to add
			var saves = setups.length;
			var add = function(game, i) {
				// create a setup 
				var s = new Setup({
					owner: p_id
					, game: game
					, name: game + " " + i
					, friendlies: false
					, tournament: t_id
				});

				// add the setup
				Database.AddSetup(s, function(err) {
					if(err) { console.log(err) }
					// wait for all of them
					saves--;
					if(saves == 0)
						done();
				}, t_id);
			}

			// for naming the setup
			var added = {};
			var tokens = [];
			setups.forEach(function(game, i) {
				added[game] = 0;
				tokens.push(game);
			});

			var countQuerys = tokens.length;
			// for each token
			tokens.forEach(function(token) {
				// get the count
				models.Setup.count({tournament:t_id, game:token}, function(err, c) {
					countQuerys--;
					if(countQuerys == 0) {
						//when all the counts are collected
						setups.forEach(function(game) {
							added[game]++;
							add(game, c + added[game]);
						});
					}
				});
			});
		}
	}
	
	// popQueue calls the next exec
	function popQueue() {
		countDebug --;
		if(countQueue.length == 0) {
			countLock = false;
		} else {
			countQueue.pop() ();
		}
		callback();
	}
	
	// create my exec
	var next = bind(popQueue);
	
	// bind my exec
	if(!countLock) {
		countLock = true;
		next();
	} else {
		countQueue.push(next);
	}
}

//override of Database.UpdateMatch()
//updates a match, and:
//	if the match has just started, record the match's start time
//	if the match being updated was just completed, also updates matches that rely on that match
//	if the match had been completed before, updates match only if passcode is valid
Database.UpdateMatch = function(callback, id, o, extra) {
	Database.Get('Match', function(err, m) {
		if(err) {
			callback(err, m);
			return;
		}
		
		if(m.state == 'open' && o.state == 'active') {
			o.started = new Date() //update time when match starts
		}
		
		// get the export
		var setExport;
		if(o.CreateExport)
			setExport = o.CreateExport();
		else
			setExport = o;	
		
		
		// update the object
		models['Match'].update({ _id: id }, { $set: setExport }, function(err, obj) {
			if(err) {
				callback(err, obj)
				return;
			}
			// if pools check
			if(m.bracket.round.bracketType == "RoundRobin" && m.bracket.round.event.currentRound == 1
				&& m.bracket.round.event.rounds.length > 1) { // currently only supports 2 rounds, maybe set this ==2
				// check if the bracket is over
				Database.CheckCompletion('Bracket', function(err, data) {
					if (err) console.log(err);
					if (data == true) {
						// set the bracket to complete
						Database.Update('Bracket', function(err, data) {
							if (err) console.log(err);
							//Database.RankPlayers(m.bracket._id, function(err, data) {
								// check if the round is complete
								Database.CheckCompletion('Round', function (err, data) {
								if (err) console.log(err);
								if (data == true) {
									// function(token, callback, id, o, extra) {
									// start the next round
									Class.Challonge.StartEvent(m.bracket.round.event._id, Class,function(err, data) {
										if (err) console.log(err);
									}, 2);
								}
								}, m.bracket.round._id)
							//});
							
							}, m.bracket._id, {complete: true});
					}
				}, m.bracket._id);
			}
			// if we're completing a match
			if(o.state == 'complete') {
				// finf the matchs who rely on this match
				models['Match'].find({ prereqMatches: m._id }, function(err, nextMatches) {
					// if it was flaged as grand finals
					console.log(o);
					if(m.isGrandFinals) {
						// is there a second set?
						//console.log(o.scoresCsv);
						var matchResult = o.scoresCsv.split('-');
						matchResult[0] = parseInt(matchResult[0]);
						matchResult[1] = parseInt(matchResult[1]);
						//console.log(matchResult[0] + " " + matchResult[1]);
						//console.log("got into grand finals");

						if(matchResult[0] > matchResult[1] || nextMatches.length == 0) {
							console.log(matchResult[0] + " " + matchResult[1]);
							console.log("got into grand finals, should not reset");
							// no second set, rank players
							Database.RankPlayers(m.bracket._id, function(err, b) {
								// give output
								Database.GivePayout(m.bracket.round.event._id, function() {
									callback(null, {finished:true});
								});
							});
							return;
						}
					}
					
					// save the ids
					var winnerId = extra.update.winnerId
					var loserId = extra.update.loserId
				
					var saves = nextMatches.length;
					if(saves == 0) {
						callback(err, nextMatches)
					}
					// for each of the next matches
					nextMatches.forEach(function(nextMatch) {
						var newState = nextMatch.state;
						// if theres only one, that means both winner and loser go forward
						// so it must be opened after this action
						if(nextMatch.players.length == 1) {
							newState='open'
						}
						
						var advance;
						var isGrandFinals = false;
						// same as above, only one
						if(nextMatches.length == 1 && m.round > 0) { 
							advance = { $each: [winnerId, loserId] };
						// winners to winners
						} else if(m.round > 0 && nextMatch.round > 0) {
							advance = winnerId;
							passedWinner = true;
						// losers to losers
						} else if(m.round < 0 && nextMatch.round < 0) {
							advance = winnerId;
						// losers to winners
						} else if(m.round < 0 && nextMatch.round > 0) {
							advance = winnerId;
							isGrandFinals = true;
						// winners to losers
						} else if(m.round > 0 && nextMatch.round < 0) {
							advance = loserId;
						}
						
						// if previous was grand finals, this must be set 2
						if(m.isGrandFinals)
							isGrandFinals = true;
						
						// assure that grand finals is open
						if(isGrandFinals)
							newState='open';
						
						// if its a new submission
						if(m.state == 'active') {
							// push the player
							models['Match'].update( {_id: nextMatch._id}, { 
								$set: { 
									state: newState 
									, isGrandFinals: isGrandFinals
								} 
								, $push: { players: advance }
							}, function(err, obj) {
								// after all saves return
								if(err) { console.log(err) }
								--saves;
								if(saves == 0) {
									callback(err, nextMatches)
								}
							});
						// resubmission
						} else {
							
							var newPlayers = nextMatch.players;
							// remove the old player we need to replace
							if(advance == winnerId) {
								newPlayers.splice(newPlayers.indexOf(loserId),1);
							} else {
								newPlayers.splice(newPlayers.indexOf(winnerId),1);
							}
							// push the new player
							newPlayers.push(advance);
							
							// push the updated match
							models['Match'].update( {_id: nextMatch._id}, { 
								$set: { 
									state: newPlayers.length == 2 ? "open" : "pending"  
									, isGrandFinals: isGrandFinals
									, players: newPlayers
								}
								
							}, function(err, obj) {
								// after all saves return
								if(err) { console.log(err) }
								--saves;
								if (saves == 0) callback(err, obj);
							});
						}
					});
				});
			
			}
			else {
				callback(err, obj)
			}
		});
	}, id, ['bracket', 'bracket.round', 'bracket.round.event', 'bracket.round.event.tournament'])
}

// NOT an override of Database.Update()
// does an argswap that updates the relative positioning of players (1 = best player, etc).
// there is a view for this
Database.UpdateSeeding = function(t_id, e_id, argswap) {
	Database.Get("Event",function(err, obj){
		var out = [];
		for(var i = 0; i < obj.seeding.length; i++)
		{
			out[i] = obj.seeding[argswap[i]];
		}
		models.Event.update({ _id: e_id }, {
			seeding: out
		}, function(err, obj){

		});

	},e_id);
}

module.exports = Database;
