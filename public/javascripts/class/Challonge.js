// Challonge API object, works very similarly to Database (singleton)
// @test are comments that can be uncommented to TEST THE CODE <---- IMPORTANT

/*
	IMPORTANT NOTE THAT CHALLONGE USES INTERNAL CHALLONGEIDS TO REFERENCE THINGS.
	we only use challonge to create the bracket to begin with, and otherwise never use
	challonge.

	challongeids do not exist in schemas, or anything outside this scope.
*/

// first, use the node-challonge module to initialize a client
// using darwin's challonge account (apikey etc can be found with each account)
var challonge = require('challonge');
var challongeClient = challonge.createClient({
		apiKey: 'JSXVEvS3ouvK01SVPGs6UGbQ7zMCgviD3x7GfFhZ',
		format: 'json',
		version: 1,
	});

// challonge object, exported with module
function Challonge() {

}

// used to update a match, report scores or in progress.
/*
	Match internals that can be updated in this way:
	{ match:
   { id: 58388449,
     state: 'complete',
     identifier: 'BL',
     round: 2,
     location: null,
     optional: false,
     tournamentId: 2388360,
     player1Id: 38226757,
     player2Id: 38226808,
     player1PrereqMatchId: null,
     player2PrereqMatchId: 58388405,
     player1IsPrereqMatchLoser: false,
     player2IsPrereqMatchLoser: false,
     winnerId: 38226757,
     loserId: 38226808,
     startedAt: '2016-04-03T23:45:57.017-04:00',
     createdAt: '2016-04-03T23:45:21.456-04:00',
     updatedAt: '2016-04-25T23:10:15.295-04:00',
     hasAttachment: false,
     player1Votes: null,
     player2Votes: null,
     groupId: null,
     attachmentCount: null,
     scheduledTime: null,
     underwayAt: null,
     rushbId: null,
     completedAt: '2016-04-03T23:45:59.423-04:00',
     suggestedPlayOrder: 64,
     prerequisiteMatchIdsCsv: '58388405',
     scoresCsv: '3-0' } }
*/
Challonge.UpdateMatch = function(challongeID, update, b_id, Class, callback) {
	/* sample update object:
		{
			scoresCsv: '3-0',
			winnerId: 10847219 <-- challongeId
		}

		note that apparently CHALLONGE API DOES NOT SUPPORT IN PROGRESS
		but we will! (on our end, so don't use updatematch for in progress)
		*/
	challongeClient.matches.update({
		id: "TOryaa_" + b_id,
		matchId: challongeID,
		match: update,
		callback: function(err,data) {
			callback(err, data);
		}
	});
}

// calls callback on a specific match, given a specific CHALLONGEID within a B_ID (bracket)
// for sample return (from console.log(data)), see comments before updateMatch
Challonge.GetMatch = function(challongeID, b_id, callback)
{
	challongeClient.matches.show({
		id: "TOryaa_" + b_id,
		matchId: challongeID,
		callback
	});
}

// commences a challonge tournament (a tournament must be STARTED before reporting matches)
Challonge.StartChallongeTournament = function(b_id, callback) {
	challongeClient.tournaments.start({
		id: String("TOryaa_" + b_id),
		callback: function(err,data){
			if (err) { console.log(err);}
			callback(err,data);
		}
	});
}

// called by Database.AddBracket, which pushes a new bracket into MongoDB. create bracket
// creates it on the challonge end. call this when things are really really finalized on the
// player list and such
Challonge.CreateBracket = function(b, callback) {
	challongeClient.tournaments.create({
	    tournament: {
	        name: String('TOryaa_' + b._id),
	        url: String('TOryaa_' + b._id),
	        tournamentType: b.bracketType
	    },
	    callback
	});
}

// adds a person to a bracket. assumed that the tournament has NOT started yet

/* sample person data
{ participant:
   { id: 39265041,
     name: '571fcdf4ff3b4140174dccf9',
     seed: 1,
     active: true,
     misc: null,
     icon: null,
     removable: true,
     username: null,
     reactivatable: false,
     group_player_ids: [],
     tournamentId: 2470096,
     createdAt: '2016-04-26T18:23:51.512-04:00',
     updatedAt: '2016-04-26T18:23:51.512-04:00',
     inviteEmail: null,
     finalRank: null,
     onWaitingList: false,
     invitationId: null,
     groupId: null,
     checkedInAt: null,
     challongeUsername: null,
     challongeEmailAddressVerified: null,
     participatableOrInvitationAttached: false,
     confirmRemove: false,
     invitationPending: false,
     displayNameWithInvitationEmailAddress: '571fcdf4ff3b4140174dccf9',
     emailHash: null,
     displayName: '571fcdf4ff3b4140174dccf9',
     attachedParticipatablePortraitUrl: null,
     canCheckIn: false,
     checkedIn: false,
	 groupPlayerIds: [] } }
*/
Challonge.AddPersonToBracket = function(b_id, p_id, callback) {
	challongeClient.participants.create({
		id: String("TOryaa_" + b_id),
		participant: {
			name: p_id
		},
		callback: callback
	});
}

// GETS ALL THE MATCHES FROM CHALLONGE, these are used to construct the bracket on our end
// NOTE THAT CHALLONGE IS NEVER CALLED EVER AGAIN. we handle match submission and everything
// locally
Challonge.GetChallongeBracketMatches = function(b_id, player_mapping, Class, callback) { 
	challongeClient.matches.index({ // get all matches from b_id bracket
		id: String("TOryaa_" + b_id),
		callback: function(err,data) {
			if (err) { console.log(err);}
			Class.Database.Get('Bracket', function(b_err, b_data) {
				if (b_err) {console.log(b_err);}
				if (b_data == null) {
					console.log("no data");
					return;
				}
				var match_mapping = {};
				var matchModels = [];
				for (var i = 0; i < data.length; i++) {
					var matchData = { // first construct a match from the given data
						state: data[i].match.state
						, players: [player_mapping[data[i].match.player1Id], player_mapping[data[i].match.player2Id]]
						, created: data[i].match.createdAt
						, round: data[i].match.round
						, bracket: b_id
						, scoresCsv: data[i].match.scoresCsv
						, playerLostPrereq: [data[i].match.player1IsPrereqMatchLoser, data[i].match.player2IsPrereqMatchLoser]
					};
					
					var mat = new Class.Match(matchData);
					console.log(mat.players);
					var m = mat.model();
					match_mapping[data[i].match.id] = m._id;
					matchModels.push(m)
				}
				
				console.log(match_mapping);
				
				for(var i = 0; i < data.length; ++i) {
					
					if(data[i].match.player1PrereqMatchId)
						matchModels[i].prereqMatches.push(match_mapping[data[i].match.player1PrereqMatchId]);
					
					if(data[i].match.player2PrereqMatchId)
						matchModels[i].prereqMatches.push(match_mapping[data[i].match.player2PrereqMatchId])	;
				
				}
				
				var saves = data.length;
				for(var i = 0; i < data.length; ++i) {
					matchModels[i].save(function(err, data) {
						if(err) {
							callback(err, obj);
							return;
						}
						
						Class.Database.UpdatePush('Bracket', function(err, data) {
							--saves;
							if(saves == 0) callback(err, data)
						}, b_id, { matches: data._id })
					});
				}
			}, b_id);
		}
	});	
}

// AN ACTUAL FUNCTION THAT THE FRONT-END SHOULD BE RUNNING
// tldr: finds event's first round, then savestarts it in a challonge bracket
Challonge.StartEvent = function(e_id, Class, callback, roundToStart) {
	var numberOfBrackets = -1; // used as saves later, used to make sure callback is only called once and on the last bracket

	var startChallongeTournamentCallback = function(bracket, player_mapping) { // called after the tournament is started, obj is a bracket
		return function(err,data){	
			//console.log('err?')
			if (err) console.log(err);
			//console.log('err.')
			// after the tournament is started, grab all the matches for MongoDB
			Class.Challonge.GetChallongeBracketMatches(bracket._id, player_mapping, Class, function(err,data){
				if (err) {console.log(err);}
				
				numberOfBrackets--;
				if (numberOfBrackets > 0) { // call callback on the last iteration
					Class.Database.Update('Event', function(err,data) {if (err) console.log(err);}, e_id, {state: "active", currentRound: roundToStart});	
				}
				else if (numberOfBrackets == 0) { Class.Database.Update('Event', callback, e_id, {state: "active", currentRound: roundToStart}); }
			});
		};
	}
	var fetchEventCallback = function(err, event_data) { // called after the first event is found
		if(event_data.seeding.length < 3) {
			callback({err: "ERROR: Not enough players to start tournament"})
			return;
		}
		Class.Database.GetNthRoundOfEvent( // when starting an event, start the first round of the event
			function(err, round_data) {
				if (err) {console.log(err);}
				var bracketType = round_data.challongeType;
				var b = { // setup bracket with given data (i.e. roundData)
					complete: false
					, round: round_data._id
					, numberOfWinners: round_data.numberOfWinners
				};
				var round = new Class.Round(round_data, Class.Bracket);
				
				var reconstructEvent = new Class.Event(event_data);
				var twoDArray = reconstructEvent.Seeding2D(event_data.seeding, round_data.entrantsPerBracket);
				numberOfBrackets = twoDArray.length; // how many times to iterate before calling overall callback!
				twoDArray.forEach(function(new_bracket) {
					var bracket = new round.BracketConstructor(b);
					Class.Database.AddBracket(bracket, // then add the bracket
						function(err, bracket){
							if(err) {
								console.log(err);
							}
							var saves = new_bracket.length; // this is to make sure that the start tournament is only called for last seed
							var mapping = {};
							new_bracket.forEach(function(seed) {
								Class.Database.AddPersonToBracket(bracket._id, seed, function(err,data){ // for each seed, add them to the bracket
										if (err) console.log(err);
										mapping[data.participant.id] = seed;
										saves--;
										if (saves == 0) {
											Class.Challonge.StartChallongeTournament(bracket._id, // then start the bracket!
												startChallongeTournamentCallback(bracket, mapping) // call callback and all that
											);
										}
									}, Class);
							}); 
						}, Class
					);
				}) 		
			}, e_id, roundToStart
		);
	}

	Class.Database.Get('Event', fetchEventCallback, e_id); // start the chain
}

module.exports = Challonge;