/*
 * This file contains the Event class
 */

//Constructor
function Event(EventExport)
{
	if(EventExport.name) this.name = EventExport.name;		// Name of the event
	if(typeof EventExport.tournament === 'undefined')
		console.log("Events must be declared with an owner!")
	this.tournament = EventExport.tournament; //name of tournament that owns this event
	if(EventExport.game) this.game = EventExport.game; //game featured in event
	if(EventExport.description) this.description = EventExport.description;	//description of event
	if(EventExport._id) this._id = EventExport._id; //database ID of event
	if(EventExport.currentRound) this.currentRound = EventExport.currentRound; //round which is currently in progress
	if(EventExport.state) this.state = EventExport.state; //either: "pending", "open", "active", "complete"
	if(EventExport.entranceFee) this.entranceFee = EventExport.entranceFee; //entry fee for this event
	if(EventExport.potBonus) this.potBonus = EventExport.potBonus; //pot bonus for this event
	
	if(EventExport.payoutSchema) { //multipliers for payouts based on placings
		payoutSchema = [];
		EventExport.payoutSchema.forEach(function(payout) {
			payoutSchema.push(payout);
		});
		this.payoutSchema = payoutSchema
	}
	
	if(EventExport.rounds) { //rounds running for event; may or may not contain exports
		var rounds = [];
		EventExport.rounds.forEach(function(round) {
			rounds.push(round);
		});
		this.rounds = rounds;
	}

	if (EventExport.seeding) { //people registered for event in seeded order; may or may not contain exports
		var seeding = [];
		EventExport.seeding.forEach(function(seed) {
			seeding.push(seed);
		});
		this.seeding = seeding;
	}
	
	return this;
}

//Body
Event.prototype = 
{
	//set default values for this object
	//however, these values MAY NOT BE HANDLED CORRECTLY by other functions handling Tournament
	name: "Missing"
	, tournament: {}
	, game: "Missing"
	, description: "Missing"
	, rounds: []
	, seeding: []
	, state: "pending"
	, currentRound: 0
	, entranceFee: 0
	, potBonus:0
	, payoutSchema: []
	
	//creates an object containing the minimum information needed to reconstruct this object from its own constructor
	//used for database storage and sometimes when passing objects
	, CreateExport: function ()
	{
		var expo = {};
		expo.name = this.name;
		expo.currentRound = this.currentRound;
		expo.state = this.state;
		expo.tournament = this.tournament;
		expo.game = this.game;
		expo.description = this.description;
		expo.currentRound = this.currentRound;
		expo.entranceFee = this.entranceFee;
		expo.potBonus = this.potBonus;
		
		if(this._id) expo._id = this._id;
		
		expo.payoutSchema = [];
		this.payoutSchema.forEach(function(payout) {
			expo.payoutSchema.push(payout);
		});
		
		expo.rounds = [];
		this.rounds.forEach(function(round) { //may contain either Rounds or RoundExports
			expo.rounds.push(round);
		});

		expo.seeding = [];
		this.seeding.forEach(function(seed) { //may contain either Persons or PersonExports
			expo.seeding.push(seed);
		});
		
		return expo;
	}
	
	/* converts a 1D array of people into a 2D array for use in multiple concurrent brackets
	 * each subarray is the seeding for one bracket
	 */
	, Seeding2D: function(seeding1d, entrantsPerBracket)
	{
		if (entrantsPerBracket < 2) entrantsPerBracket = seeding1d.length;
		var seeding2d = [];
		var numBrackets = Math.ceil(seeding1d.length / entrantsPerBracket);
		for(var i = 0; i < numBrackets; ++i) {
			seeding2d[i] = []
		}
		
		for(var i = 0; i < seeding1d.length; ++i) {
			seeding2d[i % numBrackets].push(this.seeding[i]);
		}
		
		return seeding2d;
	}
}

if(typeof module !== 'undefined') //allows frontend to access this class
{
	module.exports = Event;
}