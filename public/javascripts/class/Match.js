/*
 * This file contains the Match class
 */


//Constructor
function Match(MatchExport) // built to be constructed from a challonge obj, see above
{
	//var MatchExport = MatchExport_.match;
	if(MatchExport.state) this.state = MatchExport.state;
	if(MatchExport.round) this.round = MatchExport.round;
	if(MatchExport.setup) this.setup = MatchExport.setup;
	if(MatchExport.scoresCsv) this.scoresCsv = MatchExport.scoresCsv;
	if(MatchExport.setup) this.setup = MatchExport.setup;
	if(MatchExport._id) this._id = MatchExport._id;
	this.started = MatchExport.started || new Date();	// time started
	// stores challongeIDs of players, to get original, Database.GetPlayerByChallongeID = function(challongeId, callback)
	var players = [];
	MatchExport.players.forEach(function(player) {
		if(player)
			players.push(player);
	})
	this.players = players;
	this.bracket = MatchExport.bracket;
	
	if(MatchExport.prereqMatches) {
		var prereqs = [];
		MatchExport.prereqMatches.forEach(function(m) {
			prereqs.push(m);
		});
		this.prereqMatches = prereqs;
	}
	if(MatchExport.playerLostPrereq) {
		var lost = [];
		MatchExport.playerLostPrereq.forEach(function(l) {
			lost.push(l);
		});
		this.playerLostPrereq = lost;
	}
}

Match.prototype = 
{
	state: "Missing"
	, round: -1
	, challongeID: 0
	, started: new Date()
	, players: []
	, scoresCsv: ""
	, prereqMatches: []
	, playerLostPrereq: []
	
	// Creates an object which can be used to construct the Round
	, CreateExport: function ()
	{
		expo = {};
		expo.state = this.state;
		expo.round = this.round;
		expo.challongeID = this.challongeID;
		expo.started = this.started;
		expo.players = [];
		this.players.forEach(function(p) {
			expo.players.push(p);
		});
		expo.bracket = this.bracket;
		expo.setup = this.setup;
		expo.scoresCsv = this.scoresCsv;
		if(this._id) expo._id = this._id;
		if(this.setup) expo.setup = this.setup;
		
		return expo;
	}
	// Saves the Match to the database
	, Save: function(dbuser, dbpass)
	{
		
	}
}

if(typeof module !== 'undefined')
{
	module.exports = Match;
}