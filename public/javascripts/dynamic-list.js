// this just handles dynamic form addition for setups
$(document).ready(function(){
	$(".add-more").bind('click', function(e){
		e.preventDefault();
		var t = $(e.target);

		// grab select
		var i = 0;
		var s =  t.parent().find("select.list-input");
		if(s) {
			i = s.prop('selectedIndex');
			console.log(i);
		}

		// grab list input
		var n = t.parent().find(".list-input").clone();
		if(s) {
			n.prop("selectedIndex", i);
		}
		var gp = t.parent().parent();

		// clone and add the list
		var c = t.parent().clone();
		c.empty();
		c.append(n);
		c.hide();
		c.insertBefore(gp.children().last());
		c.slideDown(100);

		// add remove button the last item of the list
		var l = gp.children().last();
		l.find("input.list-input").val("");
		l.find("button.remove-more").show();
	});

	// remove list on click
	$(".remove-more").bind('click', function(e){
		e.preventDefault();
		var t = $(e.target);
		var gp = t.parent().parent();

		var l = gp.children().last();
		l.val(l.prev().val());
		l.prev().slideUp(100, function() {
			$(this).remove();
			if(gp.children().length == 1)
				gp.last().find("button.remove-more").hide();
		});
	}).hide();
});
