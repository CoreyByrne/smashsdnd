/*
	THIS IS THE VIEW THAT CREATES NICE BRACKET VIEWS
*/

// set width and height of svg
var width = 2000,
    height = 800;

// set width and height of graph
var cluster = d3.layout.cluster()
    .size([height, width-400]);

// create svg
var svg = d3.select("#bracket").append("svg")
    .attr("width", width)
    .attr("height", height)
		.append("g")
    .attr("transform", "translate(300,0)");

// grab json
var nodes = cluster.nodes(bracketJson);

// set up links
var link = svg.selectAll(".link")
		.data(cluster.links(nodes))
		.enter().append("path")
		.attr("class", "link")
		.attr("d", elbow);

// set up nodes
// -200 x -25 to handle the tournamentMatch section
var node = svg.selectAll(".node")
		.data(nodes)
		.enter().append("g")
		.attr("class", "node")
		.attr("transform", function(d) {
			d.y *= 1;
			return "translate(" + (d.y-200) + "," + (d.x-25) + ")";
		});

// match data for the node
// include the tournamentMatch data
node.append("foreignObject")
		.append("xhtml:div")
		.html(function(d) {
			var player1, player2;
			var score1, score2;
			var winner = '';

			// get player tags
			player1 = (d.players.length > 0) ? d.players[0].sponsor + '|' + d.players[0].tag : 'N/A';
			player2 = (d.players.length > 1) ? d.players[1].sponsor + '|' + d.players[1].tag : 'N/A';

			// get scores (if available)
			if(d.state == 'complete') {
				var regex = /\d/g;
				var scores = d.scoresCsv.match(regex);
				score1 = scores[0];
				score2 = scores[1];
				if(score1 > score2) winner = 'p1';
				else winner = 'p2';
			}

			// bolding variables
			var p1strong = (winner == 'p1') ? "<strong>" : "";
			var p1end = (winner == 'p1') ? "</strong>" : "";
			var p2strong = (winner == 'p2') ? "<strong>" : "";
			var p2end = (winner == 'p2') ? "</strong>" : "";

			// scorebox variables
			var p1box = (winner != '') ? '<div class="scoreBox">' + score1 + '</div>' : '';
			var p2box = (winner != '') ? '<div class="scoreBox">' + score2 + '</div>' : '';

			return '<div class="tournamentMatch">' +
				'<div class="tournamentPlayer0 form-inline">' +
				p1strong +
				'<div class="player">' + player1 + '</div>' +
				p1box + 
				p1end +
				'</div>' +
				'<div class="tournamentPlayer1 form-inline">' +
				p2strong +
				'<div class="player">' + player2 + '</div>' +
				p2box +
				p2end +
				'</div>' +
				'</div>' +
				'</div>';
		});

// submission elements if the match hasn't started
// this adds the submit and modal
node.filter(function(d) {return d.state == 'active';}).append("foreignObject")
		.attr("width", width)
		.attr("height", height)
		.append("xhtml:div")
		.html(function(d) {
			// add the modal for each match
			svg.append("foreignObject")
				.attr("width", width)
				.attr("height", height)
				.append("xhtml:div")
				.html(
						'<div class="modal fade" id="modal' + d.id + '" role="dialog">' +
						'<div class="vertical-alignment-helper">' +
						'<div class="modal-dialog vertical-align-center">' +
						'<div class="modal-content">' +
						'<div class="modal-header">' +
						'<button type="button" class="close" data-dismiss="modal">&times;</button>' +
						'<h4 class="modal-title">Match Submission</h4>' +
						'</div>' +
						'<div class="modal-body">' +
						'<input hidden="true" class="match" value="' + d.id + '">' +
						'<input hidden="true" class="setup" value="' + d.setup + '">' +
						'<input hidden="true" class="hidden0" value="' + d.players[0]._id + '">' +
						'<input hidden="true" class="hidden1" value="' + d.players[1]._id + '">' +
						'<strong>' + d.players[0].sponsor  + '|' + d.players[0].tag + '</strong>' +
						'<input style="margin-left:10px; width:50px; height:35px" type="number" min="-1" class="form-control player0">' +
						'<br>' +
						'<strong>' + d.players[1].sponsor  + '|' + d.players[1].tag + '</strong>' +
						'<input style="margin-left:10px; width:50px; height:35px" type="number" min="-1" class="form-control player1">' +
						'</div>' +
						'<div class="modal-footer">' +
						'<button type="button" class="btn btn-primary submitMatch">Submit</button>' +
						'<button type="button" class="btn btn-defaut" data-dismiss="modal">Close</button>' +
						'</div>' +
						'</div>' +
						'</div>' +
						'</div>' +
						'</div>'
						);
			return '<button style="margin: 12px 0 0 205px" class="btn btn-sm btn-info" data-toggle="modal" data-target="#modal' + d.id + '">Submit</button>';
		});


// round name
// print above the tournamentMatch div
node.append("text")
		.attr("dx", function(d) {return d.children ? 100 : 0})
		.attr("dy", -10)
		.attr("text-anchor", function(d) { return d.children ? "end" : "start"; })
		.text(function(d) { return d.name; });

// calculates dx, dy
function elbow(d, i) {
  return "M" + (d.source.y) + "," + d.source.x
      + "V" + (d.target.x) + "H" + (d.target.y);
}
