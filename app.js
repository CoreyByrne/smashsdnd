// requires

var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var exphbs  = require('express-handlebars');
var mongoose = require('mongoose');
var deepPopulate = require('mongoose-deep-populate')(mongoose);

// route requires

var routes = require('./routes/index');
var create = require('./routes/create');
var view = require('./routes/view');
var register = require('./routes/register');
var edit = require('./routes/edit');
var manage = require('./routes/manage');
var temp = require('./routes/temp');

// class requires

var Class = {
	Tournament: require('./public/javascripts/class/Tournament')
	, Event: require('./public/javascripts/class/Event')
	, Round: require('./public/javascripts/class/Round')
	, Bracket: require('./public/javascripts/class/Bracket')
	, Database: require('./public/javascripts/class/Database')
	, Person: require('./public/javascripts/class/Person')
	, Setup: require('./public/javascripts/class/Setup')
	, Match: require('./public/javascripts/class/Match')
	, Challonge: require('./public/javascripts/class/Challonge')
};

// handlebars helpers

var hbs = exphbs.create({
	defaultLayout: 'main'
	, helpers: {
		switch: function(value, options) {
			this._switch_value_ = value;
			var html = options.fn(this); // Process the body of the switch block
			delete this._switch_value_;
			return html;
		}
		, case:  function(value, options) {
			if (value == this._switch_value_) {
				return options.fn(this);
			}
		}
	}
});

// connect database

mongoose.connect('localhost:27017/toryaa');
Class.Database(mongoose, Schemas);
Class.Database.SetClass(Class);

// schema require

var Schemas = require('./schemas.js');

// cross reference binding

function Bind(name, Schema, Constructor) {
	Schema.methods.construct = function() {
		return new Constructor(this);
	}
	
	Schema.plugin(deepPopulate);

	var model = mongoose.model(name, Schema);

	Class.Database.AddSchemaAndModel(name, model, Schema);
	
	if(typeof Constructor === 'function')
	{
		Constructor.prototype.model = function() {
			return new model(this.CreateExport(withKey = true, withID = false));
		}
	} else if(typeof Constructor === 'object') {
		Constructor.__proto__.model = function() {
			return new model(this.CreateExport(withKey = true, withID = false));
		}
	}
}

Bind('Tournament', Schemas.Tournament, Class.Tournament);
Bind('Event', Schemas.Event, Class.Event);
Bind('Person', Schemas.Person, Class.Person);
Bind('Setup', Schemas.Setup, Class.Setup);
Bind('Match', Schemas.Match, Class.Match);
Bind('Round', Schemas.Round, Class.Round);
Bind('Bracket', Schemas.Bracket, Class.Bracket.Bracket);

// application

var app = express();

// view engine setup, this is temporary and can be set to whatever the front end wants
app.set('views', path.join(__dirname, 'views'));
app.engine('handlebars', hbs.engine);
app.set('view engine', 'handlebars');

//Defining middleware to serve static files
app.use('/static', express.static('public'));

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(function(req,res,next){
    req.Class = Class;
    next();
});
app.use('/', routes);
app.use('/create', create);
app.use('/view', view);
app.use('/register', register);
app.use('/edit', edit);
app.use('/manage', manage);
app.use('/temp', temp);

// error handlers

// development error handler
// will print stacktrace

// catch 404 and forward to error handler
/*
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});
*/

app.use(function(err, req, res, next) {
	res.status(err.status || 500);
	console.log(err);
	console.log(err.stack);
	res.send("Error, see console");
});

module.exports = app;
